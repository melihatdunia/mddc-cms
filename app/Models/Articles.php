<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Articles extends Model
{
    protected $table = 'articles';
    protected $primaryKey = 'id';
    protected $guarded = [];

    public function categories(){
        return $this->belongsTo('App\Models\Categories','id_categories','id');
    }
}
