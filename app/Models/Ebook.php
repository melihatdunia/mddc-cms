<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ebook extends Model
{
    protected $table = 'ebook';
    protected $primaryKey = 'id';
    protected $guarded = [];
}
