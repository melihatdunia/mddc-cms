<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Ebook;

use Illuminate\Support\Facades\Auth;
use App\Libraries\Utils\DateFormat;

class EbookController extends Controller
{
    public function index(){
    	$data['list'] = Ebook::all();
    	return view('admin.ebook.list', $data);
    }

    public function create(){
    	return view('admin.ebook.create');
    }

    public function store(Request $request){
        // Upload Data
        $dokFiles = [
            'cover'
        ]; 
        
        $data = [];

        foreach ($dokFiles as $file) {
            if($request->file($file) != ''){
                $data[$file] = $this->uploadFileDok($request->title, $file, $request->file($file));
            }
        }

        $data['title'] = $request->title;
        $data['ebook_link'] = $request->ebook_link;
        $data['descriptions'] = $request->descriptions;
        $data['status_publish'] = $request->status_publish;
        $data['date_publish'] =  DateFormat::convertToDateDb($request->date_publish);
        $data['author'] = Auth::user()->name;
        $data['slug'] =  strtolower(str_replace(" ","-", $request->title));

    	$simpan = Ebook::create($data);

    	if ($simpan){
    		return redirect()->route('ebooks.index')->with('alert-success', 'Data Berhasil Disimpan.');
    	}else{
    		return redirect()->route('ebooks.index')->with('alert-error', 'Data Gagal Disimpan.');
    	}
    }

    public function edit($id){
    	$data['row'] = Ebook::findOrFail($id);

      	return view('admin.ebook.edit', $data);
    }

    public function update(Request $request, $id){
    	$table = Ebook::findOrFail($id);

    	// Upload Data
        $dokFiles = [
            'cover'
        ]; 
        
        $data = [];

        foreach ($dokFiles as $file) {
            if($request->file($file) != ''){
                $data[$file] = $this->uploadFileDok($request->title, $file, $request->file($file));
            }
        }

        $data['title'] = $request->title;
        $data['ebook_link'] = $request->ebook_link;
        $data['descriptions'] = $request->descriptions;
        $data['status_publish'] = $request->status_publish;
        $data['date_publish'] =  DateFormat::convertToDateDb($request->date_publish);
        $data['author'] = Auth::user()->name;
        $data['slug'] =  strtolower(str_replace(" ","-", $request->title));

    	$ubah = $table->fill($data)->save();

    	if ($ubah){
    		return redirect()->route('ebooks.index')->with('alert-success', 'Data Berhasil Disimpan.');
    	}else{
    		return redirect()->route('ebooks.index')->with('alert-error', 'Data Gagal Disimpan.');
    	}
    }

    public function destroy($id){
        $response = ['status' => 'error', 'message' => 'Data gagal terhapus.'];

        $table = Ebook::findOrFail($id);
        if ($table->delete()) {
            $response['status'] = 'success';
        }

        return response()->json($response);
    }
    
    
    public function uploadFileDok($title, $file_name, $file)
    {

        $fileExtention  = $file->getClientOriginalExtension();
        $fileName       = $file_name.'_'.strtolower(str_replace(" ","_",$title)).'.'.$fileExtention;

        $file->move(public_path('uploads/ebook/'), $fileName);

        return $fileName;
    }

}
