<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Categories;

class CategoriesController extends Controller
{
    public function index(){
    	$data['list'] = Categories::all();
    	return view('admin.categories.list', $data);
    }

    public function create(){
    	return view('admin.categories.create');
    }

    public function store(Request $request){
    	$data = [
    		'category' 		=> $request->category
    	];

    	$simpan = Categories::create($data);

    	if ($simpan){
    		return redirect()->route('categories.index')->with('alert-success', 'Data Berhasil Disimpan.');
    	}else{
    		return redirect()->route('categories.index')->with('alert-error', 'Data Gagal Disimpan.');
    	}
    }

    public function edit($id){
    	$data['row'] = Categories::findOrFail($id);
      	return view('admin.categories.edit', $data);
    }

    public function update(Request $request, $id){
    	$table = Categories::findOrFail($id);

    	$data = [
            'category'      => $request->category
        ];

    	$ubah = $table->fill($data)->save();

    	if ($ubah){
    		return redirect()->route('categories.index')->with('alert-success', 'Data Berhasil Disimpan.');
    	}else{
    		return redirect()->route('categories.index')->with('alert-error', 'Data Gagal Disimpan.');
    	}
    }

    public function destroy($id){
        $response = ['status' => 'error', 'message' => 'Data gagal terhapus.'];

        $table = Categories::findOrFail($id);
        if ($table->delete()) {
            $response['status'] = 'success';
        }

        return response()->json($response);
    }

}
