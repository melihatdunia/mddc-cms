<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Articles;
use App\Models\Categories;

use Illuminate\Support\Facades\Auth;
use App\Libraries\Utils\DateFormat;

class ArticlesController extends Controller
{
    public function index(){
    	$data['list'] = Articles::all();
    	return view('admin.articles.list', $data);
    }

    public function create(){
        $data['listCategory'] = Categories::all();

    	return view('admin.articles.create', $data);
    }

    public function store(Request $request){
        // Upload Data
        $dokFiles = [
            'cover'
        ]; 
        
        $data = [];

        foreach ($dokFiles as $file) {
            if($request->file($file) != ''){
                $data[$file] = $this->uploadFileDok($request->title, $file, $request->file($file));
            }
        }

        $data['title'] = $request->title;
        $data['descriptions'] = $request->descriptions;
        $data['status_publish'] = $request->status_publish;
        $data['date_publish'] =  DateFormat::convertToDateDb($request->date_publish);
        $data['id_categories'] = $request->id_categories;
        $data['tags'] = $request->tags;
        $data['author'] = Auth::user()->name;
        $data['slug'] =  strtolower(str_replace(" ","-", $request->title));

    	$simpan = Articles::create($data);

    	if ($simpan){
    		return redirect()->route('articles.index')->with('alert-success', 'Data Berhasil Disimpan.');
    	}else{
    		return redirect()->route('articles.index')->with('alert-error', 'Data Gagal Disimpan.');
    	}
    }

    public function edit($id){
    	$data['row'] = Articles::findOrFail($id);
        $data['listCategory'] = Categories::all();

      	return view('admin.articles.edit', $data);
    }

    public function update(Request $request, $id){
    	$table = Articles::findOrFail($id);

    	// Upload Data
        $dokFiles = [
            'cover'
        ]; 
        
        $data = [];

        foreach ($dokFiles as $file) {
            if($request->file($file) != ''){
                $data[$file] = $this->uploadFileDok($request->title, $file, $request->file($file));
            }
        }

        $data['title'] = $request->title;
        $data['descriptions'] = $request->descriptions;
        $data['status_publish'] = $request->status_publish;
        $data['date_publish'] =  DateFormat::convertToDateDb($request->date_publish);
        $data['id_categories'] = $request->id_categories;
        $data['tags'] = $request->tags;
        $data['author'] = Auth::user()->name;
        $data['slug'] =  strtolower(str_replace(" ","-", $request->title));

    	$ubah = $table->fill($data)->save();

    	if ($ubah){
    		return redirect()->route('articles.index')->with('alert-success', 'Data Berhasil Disimpan.');
    	}else{
    		return redirect()->route('articles.index')->with('alert-error', 'Data Gagal Disimpan.');
    	}
    }

    public function destroy($id){
        $response = ['status' => 'error', 'message' => 'Data gagal terhapus.'];

        $table = Articles::findOrFail($id);
        if ($table->delete()) {
            $response['status'] = 'success';
        }

        return response()->json($response);
    }
    
    
    public function uploadFileDok($title, $file_name, $file)
    {

        $fileExtention  = $file->getClientOriginalExtension();
        $fileName       = $file_name.'_'.strtolower(str_replace(" ","_",$title)).'.'.$fileExtention;

        $file->move(public_path('uploads/articles/'), $fileName);

        return $fileName;
    }

}
