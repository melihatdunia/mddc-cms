<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\User;
use App\Constants\HakAkses;

class UserController extends Controller
{
    public function index(){
    	$data['list'] = User::all();
    	return view('admin.user.list', $data);
    }

    public function create(){
    	return view('admin.user.create');
    }

    public function store(Request $request){
    	$MailExist = User::where('email', $request->email)->count();

    	if($MailExist){
    		return redirect()->route('users.index')->with('alert-error', 'Email Sudah Pernah Ada, Silahkan Masukkan Email Lain');
    	}

    	$data = [
    		'name' 			=> $request->name,
    		'email' 		=> $request->email,
    		'password' 		=> bcrypt($request->cpassword),
    		'hak_akses'		=> $request->hak_akses,
    	];

    	$simpan = User::create($data);

    	if ($simpan){
    		return redirect()->route('users.index')->with('alert-success', 'Data Berhasil Disimpan.');
    	}else{
    		return redirect()->route('users.index')->with('alert-error', 'Data Gagal Disimpan.');
    	}
    }

    public function edit($id){
    	$data['row'] = User::findOrFail($id);
      	return view('admin.user.edit', $data);
    }

    public function update(Request $request, $id){
    	$table = User::findOrFail($id);

    	$data['name'] 	= $request->name;

    	if($request->cpassword != ""){
    		$data['password'] 	= bcrypt($request->cpassword);
    	}

    	$data['hak_akses'] 	= $request->hak_akses;

    	$ubah = $table->fill($data)->save();

    	if ($ubah){
    		return redirect()->route('users.index')->with('alert-success', 'Data Berhasil Disimpan.');
    	}else{
    		return redirect()->route('users.index')->with('alert-error', 'Data Gagal Disimpan.');
    	}
    }

    public function destroy($id){
        $response = ['status' => 'error', 'message' => 'Data gagal terhapus.'];

        $table = User::findOrFail($id);
        if ($table->delete()) {
            $response['status'] = 'success';
        }

        return response()->json($response);
    }
}
