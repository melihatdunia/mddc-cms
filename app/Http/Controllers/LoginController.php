<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Models\User;

class LoginController extends Controller
{   
    public function index(){
    	return view('login');
    }

    public function authentication(Request $request){
    	$credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            return redirect('admin');
        }else{
        	return redirect('/login');
        }
    }

    public function logout(){
    	Auth::logout();
        return redirect('/login');
    }
}
