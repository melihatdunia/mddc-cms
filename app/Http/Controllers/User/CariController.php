<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Articles;


class CariController extends Controller
{
    public function __construct()
    {

    }

    public function index(){
    	return view('user.pencarian');
    }

    private $paginate = 10;
    public function cari(Request $request){
        $data['keyword'] = $request->cari;

        $data['number'] = 1;

        $list = Articles::where('title', 'like', '%'.$data['keyword'].'%')->orderBy('date_publish', 'DESC');

        $data['total']  = $list->count();
        $data['list']   = $list->paginate($this->paginate);

        if ($request->page) {
            $data['number'] = ($request->page * $this->paginate) - $this->paginate + 1;
        }

        return view('user.hasil_pencarian', $data);
    }
}
