<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Articles;


class ArticlesController extends Controller
{
    public function __construct()
    {

    }

    public function read(Request $request, $id, $slug){
        $data['rowArtikel'] = Articles::where('id', $id)->first();

        $slugDb = $data['rowArtikel']->slug;

        if($slug == $slugDb){
                $data['prevArtikel'] = Articles::where('status_publish', 1)
            ->where('date_publish','>',$data['rowArtikel']->date_publish)
            ->orderby('date_publish','ASC')->limit(0,1)->first();

            $data['nextArtikel'] = Articles::where('status_publish', 1)
                ->where('date_publish','<',$data['rowArtikel']->date_publish)
                ->orderby('date_publish','DESC')->limit(0,1)->first();

            // Update View Article
            $table = Articles::findOrFail($id);

            $totalView = $table->view;

            $dataArticle['view'] = $totalView + 1;

            $ubah = $table->fill($dataArticle)->save();

            return view('user.read', $data);
        }else{
            return redirect('read/'.$id.'/'.$slugDb);
        }
    }
}
