<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Articles;
use App\Models\Ebook;

class HomeController extends Controller
{
    public function __construct()
    {

    }

    private $paginate = 9;

    public function index(Request $request){
        // $data['listArtikel'] = Articles::where('status_publish', 1)->orderBy('date_publish', 'DESC')->get();

        $data['number'] = 1;

        $listArtikel = Articles::where('status_publish', 1)->orderBy('date_publish', 'DESC');

        $data['total']  = $listArtikel->count();
        $data['listArtikel']   = $listArtikel->paginate($this->paginate);

        if ($request->page) {
            $data['number'] = ($request->page * $this->paginate) - $this->paginate + 1;
        }

        $data['ebookUpdate'] = Ebook::where('status_publish', 1)->orderBy('date_publish', 'DESC')->limit(1)->first();

    	return view('user.home', $data);
    }
}
