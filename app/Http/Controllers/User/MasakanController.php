<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Articles;


class MasakanController extends Controller
{
    public function __construct()
    {

    }

    private $paginate = 9;

    public function index(Request $request){
        $data['kategori'] = "Masakan";

        $data['number'] = 1;

        $list = Articles::where('status_publish', 1)->where('id_categories','7')
            ->orderBy('date_publish', 'DESC');

        $data['total']  = $list->count();
        $data['list']   = $list->paginate($this->paginate);

        if ($request->page) {
            $data['number'] = ($request->page * $this->paginate) - $this->paginate + 1;
        }

    	return view('user.sub_kanal', $data);
    }
}
