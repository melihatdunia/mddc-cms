<?php


namespace App\Exports;


use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class PenimbanganReport implements FromView
{
    private $list;

    /**
     * PermohonanAkreditasiReport constructor.
     * @param $akreditasi
     */
    public function __construct($list)
    {
        $this->list = $list;
    }

    public function view(): View
    {
        $list = $this->list;
        return view('admin.laporan.cetak_penimbangan', compact('list'));
    }
}
