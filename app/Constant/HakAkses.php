<?php


namespace App\Constant;


class HakAkses
{
    const ADMIN = 1;
    const STAFF = 2;

    public static function toSelectArray(): array
    {
        return [
            self::ADMIN => 'Admin',
            self::STAFF => 'Staff'
        ];
    }

    public static function toArray(): array
    {
        return array_flip(self::toSelectArray());
    }

    public static function getValue($key): int
    {
        $list = self::toArray();
        return $list[$key] ?? 0;

    }

    public static function getDescription($value): string
    {
        $list = self::toSelectArray();
        return $list[$value] ?? '';
    }
}
