<?php


namespace App\Constant;


class StatusPublish
{
    const PUBLISH = 1;
    const UNPUBLISH = 2;

    public static function toSelectArray(): array
    {
        return [
            self::PUBLISH => 'Publish',
            self::UNPUBLISH => 'Unpublish'
        ];
    }

    public static function toArray(): array
    {
        return array_flip(self::toSelectArray());
    }

    public static function getValue($key): int
    {
        $list = self::toArray();
        return $list[$key] ?? 0;

    }

    public static function getDescription($value): string
    {
        $list = self::toSelectArray();
        return $list[$value] ?? '';
    }
}
