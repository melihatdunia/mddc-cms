<?php


namespace App\Libraries\Utils;

use Illuminate\Support\Facades\DB;

class Autonumber
{
    public static function AutonumberNasabah(){
        $max = DB::table('nasabah')->select(DB::raw('MAX(RIGHT(urut_nasabah,3)) as kd_max'));

        if($max->count()>0)
        {
            foreach($max->get() as $row)
            {
                $tmp = ((int)$row->kd_max)+1;
                $kd = sprintf("%03s", $tmp);
            }
        }
        else
        {
            $kd = "001";
        }

        return $kd;
    }
}