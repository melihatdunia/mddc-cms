<?php


namespace App\Libraries\Utils;

class DateFormat
{
    private static $month = [
        'Januari',
        'Februari',
        'Maret',
        'April',
        'Mei',
        'Juni',
        'Juli',
        'Agustus',
        'September',
        'Oktober',
        'November',
        'Desember'
    ];

    private static $monthShort = [
        'Jan',
        'Feb',
        'Mar',
        'Apr',
        'Mei',
        'Jun',
        'Jul',
        'Agu',
        'Sep',
        'Okt',
        'Nov',
        'Des'
    ];

    public static function convertToDateId($date)
    {
        try {
            $expDateTime = explode(' ', $date);
            $expDate = explode('-', $expDateTime[0]);

            return $expDate[2] . ' ' . self::$month[(int)$expDate[1] - 1] . ' ' . $expDate[0];
        }catch (\Exception $e){
            return '';
        }
    }

    public static function convertToDateShortId($date)
    {
        try {
            $expDateTime = explode(' ', $date);
            $expDate = explode('-', $expDateTime[0]);

            return $expDate[2] . ' ' . self::$monthShort[(int)$expDate[1] - 1] . ' ' . $expDate[0];
        }catch (\Exception $e){
            return '';
        }
    }

    public static function dateMonthYearIndo($date, $withTime = false): string
    {
        try {
            $expDateTime = explode(' ', $date);
            $expDate = explode('-', $expDateTime[0]);

            $date = $expDate[2].' '.self::$month[(int)$expDate[1] - 1].' '.$expDate[0];

            if ($withTime) {
                $date = $date.' '.$expDateTime[1];
            }

            return $date;
        }catch (\Exception $e){
            return '';
        }
    }

    public static function dateMonthIndo($date, $withTime = false): string
    {
        try {
            $expDateTime = explode(' ', $date);
            $expDate = explode('-', $expDateTime[0]);

            $date = $expDate[2].' '.self::$month[(int)$expDate[1] - 1];

            if ($withTime) {
                $date = $date.' '.$expDateTime[1];
            }

            return $date;
        }catch (\Exception $e){
            return '';
        }
    }

    public static function MonthIndo($date, $withTime = false): string
    {
        try {
            $expDateTime = explode(' ', $date);
            $expDate = explode('-', $expDateTime[0]);

            $date = self::$month[(int)$expDate[1] - 1];

            if ($withTime) {
                $date = $date.' '.$expDateTime[1];
            }

            return $date;
        }catch (\Exception $e){
            return '';
        }
    }

    public static function convertToDateEn($date)
    {
        return date('F d Y', strtotime($date));
    }

    public static function convertToDateDb($date)
    {
        return ($date != "" ) ? date("Y-m-d",strtotime($date)) : null;
    }

    public static function convertToDate($date)
    {
        return ($date != "" ) ? date("d-m-Y",strtotime($date)) : null;
    }
}
