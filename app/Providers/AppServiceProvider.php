<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;

use App\Models\Articles;
use App\Models\Youtube;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        View::share('ArtikelTerbaru', Articles::where('status_publish', 1)
            ->orderBy('date_publish', 'DESC')->limit(5)->get());

        View::share('TagsPopuler', Articles::select('tags')->orderBy('view','DESC')
            ->limit(5)->get());

        View::share('YoutubeTerbaru', Youtube::where('status_publish', 1)->orderBy('date_publish','DESC')
            ->limit(3)->get());

        View::share('ArtikelTravel', Articles::where('id_categories', 3)->where('status_publish', 1)->orderBy('date_publish', 'DESC')
            ->get());

        View::share('BanyakDibaca', Articles::where('status_publish', 1)->orderBy('view','DESC')
            ->limit(10)->get());
    }

    
}
