<footer class="footer">
	<div class="container-fluid">
		<div class="copyright">
			&copy; Copyright 2021
		</div>
		<div class="copyright ml-auto">
			made with &nbsp; <i class="fa fa-heart heart text-danger"></i> &nbsp; by <a href="https://www.melihatdunia.com">Azis Saputra Isnaini</a>
		</div>				
	</div>
</footer>