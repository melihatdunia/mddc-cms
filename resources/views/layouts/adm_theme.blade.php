<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<title>Melihatdunia - Admin</title>
		<meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
		<link rel="icon" href="{{ asset('atlantis/img/icon.ico') }}" type="image/x-icon"/>

		<!-- Fonts and icons -->
		<script src="{{ asset('atlantis/js/plugin/webfont/webfont.min.js') }}"></script>
		<script>
			WebFont.load({
				google: {"families":["Lato:300,400,700,900"]},
				custom: {"families":["Flaticon", "Font Awesome 5 Solid", "Font Awesome 5 Regular", "Font Awesome 5 Brands", "simple-line-icons"], urls: ['{{ asset('atlantis/css/fonts.min.css') }}']},
				active: function() {
					sessionStorage.fonts = true;
				}
			});
		</script>
		
		<!-- CSS Files -->
		<link rel="stylesheet" href="{{ asset('atlantis/css/bootstrap.min.css') }}">
		<link rel="stylesheet" href="{{ asset('atlantis/css/atlantis.min.css') }}">
		
		<style type="text/css">
			.bg-primary-gradient{
				background: linear-gradient(-45deg,#1752c9,#275ce3) !important;
			}

			.no{
				text-align: center;
				width: 5%;
			}

			.action{
				text-align: center;
			}

			.action .btn-group a:focus{
				color: #fff;
			}

			.form-icon{
				position: relative;
			}

			.form-with-icon{
				padding: 0.6rem 3rem .6rem 1rem !important;
			}

			.field-icon{
				position: absolute;
				top: 10px;
				right: 10px;
				font-size: 22px;
			}

			.uppercase{
				text-transform: uppercase;
			}

			.text-success{
				color: #31CE36;
			}

			.text-danger{
				color: #F25961;
			}

			.btn{
				font-size: 12px !important;
			}

			.bootstrap-tagsinput{
				border: 1px solid #edeff3;
				width: 100%;
				padding: .6rem 1rem;
			}

			.bootstrap-tagsinput .tag{
				margin: 2px;
				background-color: #1269db;
				color: #fff;
				border: none;
			}

			.img-thumb{
				width:  150px;
				height: 100px;
				display:  block;
				margin:  10px auto;
				object-fit: cover;
			}

			.img-thumb-prev{
				width: 200px;
				height: 130px;
				display: block;
				margin: 10px 0px;
				object-fit: cover;
				float: right;
				border-radius: 5px;
			}
		</style>
	</head>
	<body>
		<div class="wrapper">
			<div class="main-header">
				<!-- Logo Header -->
				<div class="logo-header" data-background-color="blue">
					
					<a href="{{ URL ('/') }}" class="logo" target="_blank">
						<img src="{{ asset('images/logo-2.png') }}" alt="brand" class="navbar-brand" width="90%">
					</a>
					<button class="navbar-toggler sidenav-toggler ml-auto" type="button" data-toggle="collapse" data-target="collapse" aria-expanded="false" aria-label="Toggle navigation">
						<span class="navbar-toggler-icon">
							<i class="icon-menu"></i>
						</span>
					</button>
					<button class="topbar-toggler more"><i class="icon-options-vertical"></i></button>
					<div class="nav-toggle">
						<button class="btn btn-toggle toggle-sidebar">
							<i class="icon-menu"></i>
						</button>
					</div>
				</div>
				<!-- End Logo Header -->

				<!-- Navbar Header -->
				<nav class="navbar navbar-header navbar-expand-lg" data-background-color="blue2">
					
					<div class="container-fluid">
						<ul class="navbar-nav topbar-nav ml-md-auto align-items-center">
							<li class="nav-item dropdown hidden-caret">
								<!-- <a class="nav-link dropdown-toggle" href="#" id="notifDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<i class="fa fa-bell"></i>
									<span class="notification">0</span>
								</a> -->
								<ul class="dropdown-menu notif-box animated fadeIn" aria-labelledby="notifDropdown">
									<li>
										<div class="dropdown-title">You have 4 new notification</div>
									</li>
									<li>
										<div class="notif-scroll scrollbar-outer">
											<div class="notif-center">
												<a href="#">
													<div class="notif-icon notif-primary"> <i class="fa fa-user-plus"></i> </div>
													<div class="notif-content">
														<span class="block">
															New user registered
														</span>
														<span class="time">5 minutes ago</span> 
													</div>
												</a>
											</div>
										</div>
									</li>
									<li>
										<a class="see-all" href="javascript:void(0);">See all notifications<i class="fa fa-angle-right"></i> </a>
									</li>
								</ul>
							</li>
							<li class="nav-item dropdown hidden-caret">
								<a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#" aria-expanded="false">
									<div class="avatar-sm">
										<img src="{{ asset('images/defaultuser.png') }}" alt="..." class="avatar-img rounded-circle">
									</div>
								</a>
								<ul class="dropdown-menu dropdown-user animated fadeIn">
									<div class="dropdown-user-scroll scrollbar-outer">
										<li>
											<div class="user-box">
												<div class="avatar-lg"><img src="{{ asset('images/defaultuser.png') }}" alt="image profile" class="avatar-img rounded"></div>
												<div class="u-text">
													@if (Auth::user()->name != "")
														<h4>{{ Auth::user()->name }}</h4>
														<p class="text-muted">{{ Auth::user()->email }}</p>
													@else
						                                {{ 'Anonimous' }}
						                                {{ '' }}
						                            @endif
													
												</div>
											</div>
										</li>
										<li>
											<div class="dropdown-divider"></div>
											<a class="dropdown-item" href="#">My Profile</a>
											<a class="dropdown-item" href="{{ url('logout') }}">Logout</a>
										</li>
									</div>
								</ul>
							</li>
						</ul>
					</div>
				</nav>
				<!-- End Navbar -->
			</div>

			<!-- Sidebar -->
			@include('layouts.adm_nav')
			<!-- End Sidebar -->

			<div class="main-panel">
				@yield('content')

				@include('layouts.adm_footer')
			</div>
		</div>
		<!--   Core JS Files   -->
		<script src="{{ asset('atlantis/js/core/jquery.3.2.1.min.js') }}"></script>
		<script src="{{ asset('atlantis/js/core/popper.min.js') }}"></script>
		<script src="{{ asset('atlantis/js/core/bootstrap.min.js') }}"></script>

		<!-- jQuery UI -->
		<script src="{{ asset('atlantis/js/plugin/jquery-ui-1.12.1.custom/jquery-ui.min.js') }}"></script>
		<script src="{{ asset('atlantis/js/plugin/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js') }}"></script>

		<!-- Input Mask -->
		<script src="{{ asset('atlantis/js/plugin/inputmask/jquery.inputmask.bundle.min.js') }}"></script>

		<!-- Moment JS -->
		<script src="{{ asset('atlantis/js/plugin/moment/moment.min.js') }}"></script>

		<!-- CkEditor -->
		<script src="https://cdn.ckeditor.com/4.16.1/standard-all/ckeditor.js"></script>

		<!-- jQuery Scrollbar -->
		<script src="{{ asset('atlantis/js/plugin/jquery-scrollbar/jquery.scrollbar.min.js') }}"></script>

		<!-- Datatables -->
		<script src="{{ asset('atlantis/js/plugin/datatables/datatables.min.js') }}"></script>

		<!-- Bootstrap Toggle -->
		<script src="{{ asset('atlantis/js/plugin/bootstrap-toggle/bootstrap-toggle.min.js') }}"></script>

		<!-- Bootstrap Tagsinput -->
		<script src="{{ asset('atlantis/js/plugin/bootstrap-tagsinput/bootstrap-tagsinput.min.js') }}"></script>

		<!-- Sweet Alert -->
		<script src="{{ asset('atlantis/js/plugin/sweetalert/sweetalert.min.js') }}"></script>

		<!-- DateTimePicker -->
		<script src="{{ asset('atlantis/js/plugin/datepicker/bootstrap-datetimepicker.min.js') }}"></script>

		<!-- Select2 -->
		<script src="{{ asset('atlantis/js/plugin/select2/select2.full.min.js') }}"></script>

		<!-- Atlantis JS -->
		<script src="{{ asset('atlantis/js/atlantis.min.js') }}"></script>

		<script>
			$(".rupiah").inputmask({ 
				alias : "currency", 
				prefix: '',
				digits: 0
			});

			$('.datatables').DataTable();

			$('.select2').select2({
				theme: "bootstrap"
			});

			$('.datepicker').datetimepicker({
				format: 'DD-MM-YYYY',
				icons: {
			        time: "fa fa-clock-o",
			        date: "fa fa-calendar",
			        up: "fa fa-arrow-up",
			        down: "fa fa-arrow-down",
			        previous: "fa fa-chevron-left",
			        next: "fa fa-chevron-right",
			        today: "fa fa-clock-o",
			        clear: "fa fa-trash-o"
			    }
			});

			// Cek Harus Angka
			function isNumberKey(evt)
			{
				var charCode = (evt.which) ? evt.which : evt.keyCode;
				if (charCode != 46 && charCode > 31
				&& (charCode < 48 || charCode > 57))
				return false;
				return true;
			}

			// Global Delete
			let token = $('#token').val()
			let url = $('#baseUrl').val()

			$('.card-body').on('click', '.hapus', function () {
			    swal({
			        title: 'Anda Yakin?',
			        text: "Mau menghapus data ini!",
			        type: 'warning',
			        buttons: {
			            confirm: {
			                text: 'Ya, hapus!',
			                className: 'btn btn-danger'
			            },
			            cancel: {
			                visible: true,
			                className: 'btn btn-primary'
			            }
			        }
			    }).then((Delete) => {
			        if (Delete) {
			            let idData = $(this).data('id')
			            deleteData(idData)
			        } else {
			            swal.close();
			        }
			    });
			});

			function deleteData(idData) {
			    $.ajax({
			        url: url + '/' + idData,
			        data: {
			            "_token": token
			        },
			        type: "DELETE",
			        success: function (resp) {
			            if (resp.status == 'success') {
			                swal({
			                    title: 'Terhapus!',
			                    text: 'Data berhadil dihapus.',
			                    icon: "success",
			                    buttons: {
			                        confirm: {
			                            className: 'btn btn-success'
			                        }
			                    }
			                }).then(function () {
			                        location.reload();
			                    }
			                );
			            } else {
			                swal({
			                    title: 'Gagal!',
			                    text: resp.message,
			                    icon: "error",
			                    buttons: {
			                        confirm: {
			                            className: 'btn btn-danger'
			                        }
			                    }
			                });
			            }
			        },
			        error: function (xhr, status, error) {
			        }
			    });
			}

			// Global Alert 
			let alertSuccess = '{!! Session::get('alert-success') !!}'
			let alertError = '{!! Session::get('alert-error') !!}'

			if(alertSuccess != ''){
				showAlertSuccess(alertSuccess)
			}else if(alertError != ''){
				showAlertError(alertError)
			}

			function showAlertSuccess(message) {
				swal(message, {
					icon: "success",
					buttons: {
						confirm: {
							className: 'btn btn-success'
						}
					}, function() {
			            location.reload()
			        }
				}).then((Ok) => {
	                location.reload()
	            });
			}

			function showAlertError(message) {
				swal(message, {
					icon: "error",
					buttons: {
						confirm: {
							className: 'btn btn-danger'
						}
					},
				}).then((Ok) => {
	                location.reload()
	            });
			}
		</script>

		@yield('javascript')
	</body>
</html>