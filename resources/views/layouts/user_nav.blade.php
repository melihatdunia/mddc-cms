<header id="navbar" class="desktop">
	<div class="container clearfix">
		<div class="head-logo">
			<img src="{{ asset('frontend/images/logo.png') }}">
		</div>
		<div class="head-nav">
			<nav>
				<ul class="navigation">
					<li>
						<a href="{{ url('/') }}" 
							data-hover="Home" 
							class="{{ @$active == 'home' ? 'active' : '' }}">
							Home
						</a>
					</li>
					<li>
						<a href="{{ url('programming') }}" 
							data-hover="Programming" 
							class="{{ @$active == 'programming' ? 'active' : '' }}">
							Programming
						</a>
					</li>
					<li>
						<a href="{{ url('traveling') }}" 
						data-hover="Traveling"
						class="{{ @$active == 'traveling' ? 'active' : '' }}">
							Traveling
						</a>
					</li>
					<li>
						<a href="{{ url('tips') }}" 
						data-hover="Tips"
						class="{{ @$active == 'tips' ? 'active' : '' }}">
							Tips
						</a>
					</li>
					<li>
						<a href="{{ url('masakan') }}" 
						data-hover="Masakan"
						class="{{ @$active == 'masakan' ? 'active' : '' }}">
							Masakan
						</a>
					</li>
					<li>
						<a href="{{ url('ebook') }}" 
						data-hover="Ebook"
						class="{{ @$active == 'ebook' ? 'active' : '' }}">
							Ebook
						</a>
					</li>
				</ul>
			</nav>
		</div>
		<div class="head-search">
			<a href="{{ url('cari') }}" class="btn-animate">
				<span>Cari</span> <i class="fas fa-search icon-search"></i>
			</a>
		</div>
	</div>
</header>
<div id="nav-fixed" class="nav--">&nbsp;</div>

<div class="mobile">
	<div class="hide-body" id="hide-body">
	</div>
	<!-- Header Navigation -->
	<div class="warp-header">
		<div class="container">
			<div class="row-content">
				<div id="nav-icon" class="nav-icon" onclick="navhead(this)">
					<div class="bar1"></div>
					<div class="bar2"></div>
					<div class="bar3"></div>
				</div>
				<div class="logo-head">
					<a href="{{ url('/') }}">
						<img src="{{ asset('frontend/images/logo.png') }}" alt="logo">
					</a>
				</div>
				<div class="search-icon">
					<a href="{{ url('cari') }}">
						<i class="fa fa-search" aria-hidden="true"></i>
					</a>
				</div>
			</div>
		</div>
	</div>

	<!-- Nav Slide Secondary -->
	<div id="nav-slide" class="nav-slide">
		<div class="nav-slide-primary">
			<a href="{{ url('/') }}">HOME</a>
			<a href="{{ url('programming') }}">PROGRAMMING</a>
			<a href="{{ url('traveling') }}">TRAVELLING</a>
			<a href="{{ url('tips') }}">TIPS</a>
			<a href="{{ url('opini') }}">OPINI</a>
			<a href="{{ url('ebook') }}">EBOOK</a>
		</div>
		<div class="nav-slide-social">
			Ikuti Kami

			<ul>
				<li>
					<a href="https://www.instagram.com/melihat_dunia_" class="social-ig" target="_blank">
						<img src="{{ asset('frontend/images/footer-ig.png') }}">
					</a>
				</li>
				<li>
					<a href="https://www.youtube.com/channel/UCM0OwTjnu16MIdsYJQnHXPw" class="social-yt" target="_blank">
						<img src="{{ asset('frontend/images/footer-yt.png') }}">
					</a>
				</li>
			</ul>
		</div>
	</div>	
</div>