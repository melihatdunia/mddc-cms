<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="x-dns-prefetch-control" content="on">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="mobile-web-app-capable" content="yes">
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=0">
		<meta name="theme-color" content="#367cbf">
		
		<title>Melihatdunia - Life Is Jurney</title>
		<meta name="title" content=">Melihatdunia - Life Is Jurney">
		<meta name="description" content=">Melihatdunia - Life Is Jurney">
		<meta name="author" content="melihatdunia.com">
		<meta name="image_src" content="">
		<meta name="google-site-verification" content="">
		<meta name="msvalidate.01" content="">
		
		<meta name="googlebot-news" content="index,follow">
		
		<link rel="shortcut icon" href="assets/images/favicon.ico">
		<link rel="icon" type="image/png" href="{{ asset('frontend/images/favicon-192x192.png') }}" sizes="192x192">
        <link rel="icon" type="image/png" href="{{ asset('frontend/images/favicon-152x152.png') }}" sizes="160x160">
        <link rel="icon" type="image/png" href="{{ asset('frontend/images/favicon-96x96.png') }}" sizes="96x96">
        <link rel="icon" type="image/png" href="{{ asset('frontend/images/favicon-32x32.png') }}" sizes="32x32">
        <link rel="icon" type="image/png" href="{{ asset('frontend/images/favicon-16x16.png') }}" sizes="16x16">

		<link rel="stylesheet" href="{{ asset('frontend/css/styles.css') }}">
		<link rel="stylesheet" href="{{ asset('frontend/css/bootstrap.min.css') }}">
		<link rel="stylesheet" href="{{ asset('frontend/font/fontawesome5/css/all.min.css') }}">

		<!-- Link Swiper's CSS -->
  		<link rel="stylesheet" href="{{ asset('frontend/plugin/swipper/swiper-bundle.min.css') }}">

  		<link href="{{ asset('frontend/plugin/prism/prism.css') }}" rel="stylesheet" />

  		<script type="text/javascript" src="https://platform-api.sharethis.com/js/sharethis.js#property=60fe6c81c18c960012ecfb9a&product=inline-share-buttons" async="async"></script>


	
	</head>

	<body>
		<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v11.0&appId=201631296572503&autoLogAppEvents=1" nonce="IOD8J4Km"></script>

		@include('layouts.user_nav')

		@php($DateFormat = 'App\Libraries\Utils\DateFormat')

		<main>
			<div class="container">
				<div class="clearfix">
					<div class="left-content">
						@yield('content')
					</div>
					<!-- End Left Content -->

					<div class="right-content">
						@include('layouts.user_side')
					</div>
					<!-- End Right Content -->
				</div>
			</div>

			<div class="travelling clearfix">
				<div class="container">
					<div class="clearfix">
						<div class="title-travelling-section">
							<i class="fas fa-suitcase-rolling"></i> Traveling
						</div>
						<div class="more-artikel">
							<a href="{{ url('traveling') }}"> Lainnya <i class="fas fa-arrow-right"></i> </a>
						</div>
					</div>
					<div class="box-artikel-travelling">
						<div class="swiper-container">
							<div class="swiper-wrapper">
								@foreach($ArtikelTravel as $rowTravelArtikel)
								<article class="list-artikel-travelling clearfix swiper-slide">
									<div class="artikel-travelling">
										<a href="{{ url('read/'.$rowTravelArtikel->id.'/'.$rowTravelArtikel->slug) }}">
											<div class="img-artikel-travelling">
												<img src="{{ asset('uploads/articles/'.$rowTravelArtikel->cover) }}" alt="img-artikel">
											</div>
											<div class="detail-artikel-travelling">
												<div class="title-artikel-travelling">
													{{ $rowTravelArtikel->title }}
												</div>
												<div class="box-auth">
													<span class="author">{{ ucfirst($rowTravelArtikel->author) }}</span>
													<span class="date">
														{{ $DateFormat::convertToDateShortId($rowTravelArtikel->date_publish) }}
													</span>
												</div>
											</div>
										</a>
									</div>
								</article>
								@endforeach
							</div>
						</div>
						<!-- Add Arrows -->
						<div class="swiper-button-next"></div>
						<div class="swiper-button-prev"></div>
					</div>
				</div>
			</div>

			<!-- Btn Back Top -->
			<button onclick="topFunction()" id="Btn2Top" class="btn-2top" title="to top">
				<i class="fa fa-chevron-up"></i>
			</button>
		</main>

		@include('layouts.user_footer')
	</body>

	<script src="{{ asset('frontend/js/jquery.min.js') }}"></script>
  	<script src="{{ asset('frontend/js/bootstrap.min.js') }}"></script>
  	<script src="{{ asset('frontend/font/fontawesome5/js/all.min.js') }}"></script>

  	<script src="{{ asset('frontend/plugin/prism/prism.js') }}"></script>

  	<!-- Swiper JS -->
  	<script src="{{ asset('frontend/plugin/swipper/swiper-bundle.min.js') }}"></script>

  	<script>
  		window.onscroll = function() {
			navsticky()
			scrollFunction()
		};

  		var swiper = new Swiper('.swiper-container', {
  			slidesPerView: "auto",
      		spaceBetween: 20,
      		// loop: true,
			navigation: {
				nextEl: '.swiper-button-next',
				prevEl: '.swiper-button-prev',
			},
			breakpoints: {
				"@0.00": {
					slidesPerView: 1,
					spaceBetween: 10,
				},
				"@0.75": {
					slidesPerView: 2,
					spaceBetween: 20,
				},
				"@1.00": {
					slidesPerView: 3,
					spaceBetween: 20,
				},
				"@1.50": {
					slidesPerView: 4,
					spaceBetween: 20,
				},
			},
	    });

	    // Open Side Navigasi
		function navhead(x) {
			// document.getElementById("nav-slide").style.display = "block";
			var nav = x.classList.toggle("change");

			if(nav == true){
				document.getElementById("nav-slide").style.top = "80px";
			}else{
				document.getElementById("nav-slide").style.top = "-1500px";
			}
		}

		// Nav sticky
		var navbar 		= document.getElementById("navbar");
		var navbarfix 	= document.getElementById("nav-fixed");
		var sticky 		= navbar.offsetTop;

		function navsticky() {

			if (window.pageYOffset >= 400) {
				navbar.classList.add("sticky");
				navbarfix.classList.add("nav-fixed");
				navbarfix.classList.remove("nav--");
			}
			if (window.pageYOffset <= 0) {
				navbar.classList.remove("sticky");
				navbarfix.classList.remove("nav-fixed");
				navbarfix.classList.add("nav--");
			}
		}

		// Menu Flip
  		(function($){
		    $.fn.flip = function(options){
				var options = $.extend({
					targetClass: '.m-flip_item'
				}, options);
				
		        return this.each(function(){
			        console.log(this);
					var $this = $(this),
						$target = $this.find(options.targetClass);
					
					$this
						.on({
							'init.flip': function(){
								var targetFirst_height = $target.eq(0).height();
								
								$this
									.data('height', targetFirst_height)
									.css({ height: targetFirst_height });
							},
							'mouseenter.flip': function(){
								$target.css({ top: -$this.data('height') + 'px' });
							},
							'mouseleave.flip': function(){
								$target.css({ top: 0 + 'px' });
							}
						})
						.trigger('init.flip');
				});
		    };
		}(jQuery));

		$(function(){
			$('.js-flip').flip();
		});

		// Back To Top
		btnTop = document.getElementById("Btn2Top");

		// Jika Scroll Opacity Turun
		$.fn.scrollEnd = function(callback, timeout) {          
			$(this).scroll(function(){
				var $this = $(this);
				if ($this.data('scrollTimeout')) {
					btnTop.style.opacity = "0.2";
					clearTimeout($this.data('scrollTimeout'));
				}
				$this.data('scrollTimeout', setTimeout(callback,timeout));
			});
		};

		// Jika scroll berenti, opacity naik
		$(window).scrollEnd(function(){
		    btnTop.style.opacity = "1";
		});

		function scrollFunction() {
			if (document.body.scrollTop > 400 || document.documentElement.scrollTop > 400) {
				btnTop.style.display = "block";
			} else {
				btnTop.style.display = "none";
			}
		}

		function topFunction() {
			$("html, body").animate({scrollTop: 0}, 500);
		}
  	</script>
</html>