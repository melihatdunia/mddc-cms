<footer>
	<div class="container">
		<div class="clearfix">
			<div class="footer-col-bio">
				<img src="{{ asset('frontend/images/logo.png') }}" class="logo-footer">
				<div class="desc-blog">
					Melihatdunia adalah blog untuk berbagi pengalaman kami tentang programming, travelling, dan hal unik yang ada didunia ini. <br><br>

					- Let's Enjoy This Blog
				</div>
			</div>
			<div class="footer-col">
				<div class="title-footer">
					Social Media
				</div>
				<div class="social-footer">
					<ul>
						<li>
							<a href="https://www.instagram.com/melihat_dunia_" target="_blank">
								<img src="{{ asset('frontend/images/footer-ig.png') }}"> @melihat_dunia_ 
							</a>
						</li>
						<li>
							<a href="https://www.youtube.com/channel/UCM0OwTjnu16MIdsYJQnHXPw" target="_blank">
								<img src="{{ asset('frontend/images/footer-yt.png') }}"> Melihatdunia 
							</a>
						</li>
					</ul>
				</div>
			</div>
			<div class="footer-col">
				<div class="title-footer">
					Kontak
				</div>
				<div class="kontak-footer">
					<table width="100%">
						<tr>
							<td width="15%" style="padding-bottom: 10px;">
								<i  style="font-size: 20px; color: #7c7c7c; margin-top: 3px;" class="far fa-envelope fa-fw"></i>
							</td>
							<td width="85%">admin@melihatdunia.com</td>
						</tr>
						<tr>
							<td>
								<i style="font-size: 20px; color: #7c7c7c; margin-top: 3px;" class="fas fa-map-marker-alt fa-fw"></i>
							</td>
							<td>Jl. Masjid Raya, Larangan Selatan, Tangerang, 151514</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<div class="copy">
			&copy; 2021 Melihatdunia - Made With <span style="color: #f23030; font-size: 16px;">&#10084;</span> For You.
		</div>
	</div>
</footer>