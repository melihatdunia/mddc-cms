<div class="sidebar sidebar-style-2">			
	<div class="sidebar-wrapper scrollbar scrollbar-inner">
		<div class="sidebar-content">
			<div class="user">
				<div class="avatar-sm float-left mr-2">
					<img src="{{ asset('images/defaultuser.png') }}" alt="..." class="avatar-img rounded-circle">
				</div>
				<div class="info">
					<a data-toggle="collapse" href="#collapseExample" aria-expanded="true">
						<span>
							@if (Auth::user()->name != "")
								{{ strtoupper(Auth::user()->name) }}
							@else
                                {{ 'Anonimous' }}
                            @endif
							<span class="user-level">Administrator</span>
						</span>
					</a>
				</div>
			</div>

			<!-- Navigasi -->
			<ul class="nav nav-primary">
				<li class="nav-item {{ @$activeMenu == 'dashboard' ? 'active' : '' }}">
					<a href="{{ url('admin') }}">
						<i class="fas fa-home"></i>
						<p>Dashboard</p>
					</a>
				</li>

				<!-- Master -->
				<li class="nav-section">
					<span class="sidebar-mini-icon">
						<i class="fa fa-ellipsis-h"></i>
					</span>
					<h4 class="text-section">Masters</h4>
				</li>

				<li class="nav-item {{ @$activeMenu == 'users' ? 'active' : '' }}">
					<a href="{{ url('users') }}">
						<i class="fas fa-user"></i>
						<p>Users</p>
					</a>
				</li>

				<li class="nav-item {{ @$activeMenu == 'categories' ? 'active' : '' }}">
					<a href="{{ url('categories') }}">
						<i class="fas fa-bookmark"></i>
						<p>Categories</p>
					</a>
				</li>
				
				<!-- Article -->
				<li class="nav-section">
					<span class="sidebar-mini-icon">
						<i class="fa fa-ellipsis-h"></i>
					</span>
					<h4 class="text-section">Article</h4>
				</li>

				<li class="nav-item {{ @$activeMenu == 'articles' ? 'active' : '' }}">
					<a href="{{ url('articles') }}">
						<i class="fas fa-edit"></i>
						<p>Articles</p>
					</a>
				</li>

				<li class="nav-item {{ @$activeMenu == 'ebooks' ? 'active' : '' }}">
					<a href="{{ url('ebooks') }}">
						<i class="fas fa-book"></i>
						<p>Ebooks</p>
					</a>
				</li>

				<li class="nav-item {{ @$activeMenu == 'youtube' ? 'active' : '' }}">
					<a href="{{ url('youtube') }}">
						<i class="fab fa-youtube"></i>
						<p>Youtube Video</p>
					</a>
				</li>
			</ul>
		</div>
	</div>
</div>