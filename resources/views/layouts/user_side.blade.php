@php($DateFormat = 'App\Libraries\Utils\DateFormat')

<!-- Artikel Baru -->
<div class="artikel-baru">
	<div class="title-side">
		Artikel Baru
	</div>

	@foreach($ArtikelTerbaru as $rowTerbaru)
	<article class="list-artikel-side clearfix">
		<a href="{{ url('read/'.$rowTerbaru->id.'/'.$rowTerbaru->slug) }}">
			<div class="img-artikel-side">
				<img src="{{ asset('uploads/articles/'.$rowTerbaru->cover) }}" alt="news-img">
			</div>
			<div class="detail-artikel-side">
				<div class="title-artikel-side">
					{{ $rowTerbaru->title }}
				</div>
				<div class="date-artikel-side">
					{{ $DateFormat::convertToDateId($rowTerbaru->date_publish) }}
				</div>
			</div>
		</a>
	</article>
	@endforeach
</div>

<!-- Tag Populer -->
<div class="tag-populer">
	<div class="title-side">
		Tag Populer
	</div>
	<div class="list-tag-populer">
		<ul>
			@php($allTags = [])

			@foreach($TagsPopuler as $tag)

				@php($vTags = explode(',', $tag->tags)) {{-- Pisahkan Tags --}}

				@php($allTags = array_merge($allTags, $vTags)) {{-- Gabungkan Tags Jadi Satu Array--}}

			@endforeach
			
			@php( $CekDuplicateTags = array_unique($allTags)) {{-- Cek Duplicate Tags --}}

			@foreach($CekDuplicateTags as $key => $rowTag)
				<li>
					<a href="#">
						{{ $CekDuplicateTags[$key] }}
					</a>
				</li>
			@endforeach
		</ul>
	</div>
</div>

<!-- Ads -->
<div class="ads-rectangle">
	<img src="{{ asset('images/300x250.jpg') }}">
</div>

<!-- Video -->
<div class="video">
	<div class="title-side">
		Video
	</div>
	<div class="headline-video">
		<a href="{{ url($YoutubeTerbaru[0]->youtube_link) }}" target="_blank">
			<img src="{{ asset('uploads/youtube/'.$YoutubeTerbaru[0]->cover) }}" alt="img-youtube">
			<div class="headline-detail-video">
				<div class="title-video">
					{{ $YoutubeTerbaru[0]->title }}
				</div>
				<div class="date-video">
					{{ $DateFormat::convertToDateId($YoutubeTerbaru[0]->date_publish) }}
				</div>
			</div>
		</a>
	</div>
	@foreach($YoutubeTerbaru as $keyYoutube => $rowYoutube)
		@if($keyYoutube != 0)
		<div class="list-video">
			<a href="{{ url($YoutubeTerbaru[$keyYoutube]->youtube_link) }}" target="_blank">
				<div class="img-list-video">
					<img src="{{ asset('uploads/youtube/'.$YoutubeTerbaru[$keyYoutube]->cover) }}" alt="img-youtube">
				</div>
				<div class="list-detail-video">
					<div class="title-video">
						{{ $YoutubeTerbaru[$keyYoutube]->title }}
					</div>
					<div class="date-video">
						{{ $DateFormat::convertToDateId($YoutubeTerbaru[$keyYoutube]->date_publish) }}
					</div>
				</div>
			</a>
		</div>
		@endif
	@endforeach
	<div class="more-youtube">
		<a href="https://www.youtube.com/channel/UCM0OwTjnu16MIdsYJQnHXPw" target="_blank">
			More in <i class="fab fa-youtube"></i> YoutTube
		</a>
	</div>
</div>

<!-- Artikel Populer -->
<div class="artikel-populer">
	<div class="title-side">
		Paling Banyak Dibaca
	</div>
	<div class="warp-list-artikel-populer">
		<ul>
			@foreach($BanyakDibaca as $key => $rowBanyakDibaca)
			<li class="list-artikel-populer">
				<span class="no-populer">#{{ $key+1 }}</span>
				<span class="title-populer">
					<a href="{{ url('read/'.$rowBanyakDibaca->id.'/'.$rowBanyakDibaca->slug) }}">
						{{ $rowBanyakDibaca->title }}
					</a>
				</span>
			</li>
			@endforeach
		</ul>
	</div>
</div>