@extends('layouts.adm_theme', ['activeMenu' => 'dashboard'])

@php($Currency = 'App\Libraries\Utils\Currency')

@section('content')
<div class="content">
	<div class="panel-header bg-primary-gradient">
		<div class="page-inner py-5">
			<div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
				<div>
					<h2 class="text-white pb-2 fw-bold">Dashboard</h2>
				</div>
			</div>
		</div>
	</div>
	<div class="page-inner mt--5">
		<div class="row mt--2">
			<div class="col-sm-12 col-md-12">
				<div class="card card-stats card-round">
					<div class="card-body ">
						<img src="{{ asset('images/logo.png') }}" 
						style="display: block; margin: auto; width: 50%; padding: 100px 0px;">
					</div>
				</div>
			</div>	
		</div>
	</div>
</div>
@endsection

@section('javascript')
@endsection