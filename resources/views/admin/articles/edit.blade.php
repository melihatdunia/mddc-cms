@extends('layouts.adm_theme', ['activeMenu' => 'articles'])

@php($DateFormat = 'App\Libraries\Utils\DateFormat')

@section('content')
<div class="content">
	<div class="panel-header bg-primary-gradient">
		<div class="page-inner py-5">
			<div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
				<div>
					<h2 class="text-white pb-2 fw-bold">Ubah Data</h2>
				</div>
				<div class="ml-md-auto py-2 py-md-0">
					<a href="{{ url('articles') }}" class="btn btn-white btn-border btn-round mr-2">
						<i class="fas fa-arrow-left mr-2"></i> Kembali ke List
					</a>
				</div>
			</div>
		</div>
	</div>
	<div class="page-inner mt--5">
		<div class="row mt--2">
			<div class="col-md-12">
				<div class="card full-height">
					<div class="card-body">
						<form action="{{ route('articles.update', $row->id) }}" method="POST" enctype="multipart/form-data">
							{{ method_field('PATCH') }}
		                    {{ csrf_field() }}
		                    <div class="card-body">
		                        <div class="row">
		                            <div class="col-md-8">
		                                <div class="form-group form-show-validation row">
		                                    <div class="col-md-12">
		                                        <input type="text" class="form-control" name="title" placeholder="Enter Title" value="{{ $row->title }}" required>
		                                    </div>
		                                </div>
		                                <div class="form-group form-show-validation row">
		                                    <div class="col-md-12">
		                                        <textarea id="editor1" name="descriptions" required>
		                                        	{!! $row->descriptions !!}
		                                        </textarea>
		                                    </div>
		                                </div>
		                            </div>
		                            <div class="col-md-4">
		                            	<div class="form-group form-show-validation row">
		                            		<label class="col-md-4 mt-sm-2 text-right">
		                            			Status
		                            		</label>
		                            		<div class="col-md-8">
			                            		<select class="form-control select2" name="status_publish">
			                            			<option value="1" {{ ($row->status_publish == 1) ? "selected" : "" }}>Published</option>
			                            			<option value="2" {{ ($row->status_publish == 2) ? "selected" : "" }}>Un Published</option>
			                            		</select>
			                            	</div>
		                                </div>

		                                <div class="form-group form-show-validation row">
		                            		<label class="col-md-4 mt-sm-2 text-right">
		                            			Publish On
		                            		</label>
		                            		<div class="col-md-8">
		                            			<input type="text" class="form-control datepicker" name="date_publish" placeholder="Date Publish" value="{{ $DateFormat::convertToDate($row->date_publish) }}" required>
		                            		</div>
		                                </div>

		                                <div class="form-group form-show-validation row">
		                            		<label class="col-md-4 mt-sm-2 text-right">
		                            			Category
		                            		</label>
		                            		<div class="col-md-8">
			                            		<select class="form-control select2" name="id_categories">
			                            			@foreach($listCategory as $rowCategory)
			                            				<option value="{{ $rowCategory->id }}" {{ ($row->id_categories == $rowCategory->id) ? "selected" : "" }}>{{ $rowCategory->category }}</option>
			                            			@endforeach
			                            		</select>
			                            	</div>
		                                </div>

		                                <div class="form-group form-show-validation row">
		                            		<label class="col-md-4 mt-sm-2 text-right">
		                            			Tags
		                            		</label>
		                            		<div class="col-md-8">
			                            		<input type="text" id="tagsinput" class="form-control" data-role="tagsinput" name="tags" value="{{ $row->tags }}">
			                            	</div>
		                                </div>

		                                <div class="form-group form-show-validation row">
		                            		<label class="col-md-4 mt-sm-2 text-right">
		                            			Cover
		                            		</label>
		                            		<div class="col-md-8">
			                            		<input type="file" class="form-control" name="cover">
			                            	</div>

			                            	<div class="col-md-12">
												<img src="{{ asset('uploads/articles/'.$row->cover) }}" class="img-thumb-prev">
											</div>
		                                </div>
		                            </div>
		                        </div>
		                    </div>
		                    <div class="card-action">
		                        <div class="row">
		                            <div class="col-md-12">
		                                <button class="btn btn-primary btn-round float-right" type="submit">
		                                	Simpan Data
		                                </button>
		                            </div>
		                        </div>
		                    </div>
		                </form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('javascript')
<script>
	// CkEditor
	var config = {
      extraPlugins: 'codesnippet, justify, font, colorbutton, smiley',
      codeSnippet_theme: 'monokai_sublime',
      format_tags: 'h1;h2;h3;h4;h5;h6;pre',
      height: 356
    };
    
    CKEDITOR.replace('descriptions', config);
</script>
@endsection