@extends('layouts.adm_theme', ['activeMenu' => 'articles'])

@php($Publish = 'App\Constant\StatusPublish')
@php($DateFormat = 'App\Libraries\Utils\DateFormat')

@section('content')
<div class="content">
	<div class="panel-header bg-primary-gradient">
		<div class="page-inner py-5">
			<div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
				<div>
					<h2 class="text-white pb-2 fw-bold">List Article</h2>
				</div>
				<div class="ml-md-auto py-2 py-md-0">
					<a href="{{ url('articles/create') }}" class="btn btn-white btn-border btn-round mr-2">
						<i class="fas fa-plus mr-2"></i> Tambah Data
					</a>
				</div>
			</div>
		</div>
	</div>
	<div class="page-inner mt--5">
		<div class="row mt--2">
			<div class="col-md-12">
				<div class="card full-height">
					<div class="card-body">
						<div class="table-responsive">
							<table class="display table table-head-bg-primary table-striped table-hover datatables">
								<thead>
									<tr>
										<th class="no">No.</th>
										<th>Thumb</th>
										<th>Title</th>
										<th>Category</th>
										<th>Date</th>
										<th class="action">Action</th>
									</tr>
								</thead>
								<tbody>
									@foreach($list as $key => $val)
									<tr>
										<td class="no">{{ $key+1 }}.</td>
										<td>
											<img class="img-thumb" src="{{ asset('uploads/articles/'. $val->cover) }}">
										</td>
										<td>
											{{ $val->title }} 
											<b>{{ ($val->status_publish == 2) ? '- '.$Publish::getDescription($val->status_publish) : "" }}</b></td>
										<td>{{ $val->categories->category }}</td>
										<td>{{ $DateFormat::convertToDate($val->date_publish) }}</td>
										<td class="action">
											<div class="btn-group" role="group" aria-label="Basic example">
	                                            <a href="{{ url('articles/'. $val->id .'/edit') }}"
	                                               class="btn btn-primary btn-sm" data-toggle="tooltip"
	                                               title="Ubah Data"><i class="fas fa-edit"></i>
	                                            </a>
	                                            <button class="btn btn-danger btn-sm hapus" data-toggle="tooltip"
	                                                title="Hapus Data" data-id="{{ $val->id }}">
	                                                <i class="fas fa-trash-alt"></i>
	                                            </button>
	                                        </div>
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- For Delete Data Url -->
<input type="hidden" id="baseUrl" value="{{ url('articles') }}">
<input type="hidden" id="token" value="{{ csrf_token() }}">
@endsection

@section('javascript')

@endsection