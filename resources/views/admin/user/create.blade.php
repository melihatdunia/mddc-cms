@extends('layouts.adm_theme', ['activeMenu' => 'users'])

@section('content')
<div class="content">
	<div class="panel-header bg-primary-gradient">
		<div class="page-inner py-5">
			<div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
				<div>
					<h2 class="text-white pb-2 fw-bold">Tambah Data</h2>
				</div>
				<div class="ml-md-auto py-2 py-md-0">
					<a href="{{ url('users') }}" class="btn btn-white btn-border btn-round mr-2">
						<i class="fas fa-arrow-left mr-2"></i> Kembali ke List
					</a>
				</div>
			</div>
		</div>
	</div>
	<div class="page-inner mt--5">
		<div class="row mt--2">
			<div class="col-md-12">
				<div class="card full-height">
					<div class="card-body">
						<form action="{{ route('users.store') }}" method="POST">
		                    {{ csrf_field() }}
		                    <div class="card-body">
		                        <div class="row">
		                            <div class="col-md-12">
		                                <div class="form-group form-show-validation row">
		                                    <label class="col-md-2 mt-sm-2 text-right">
		                                        Email <span class="required-label">*</span>
		                                    </label>
		                                    <div class="col-md-4">
		                                        <input type="email" class="form-control" name="email" placeholder="Email" required>
		                                    </div>
		                                </div>
		                                <div class="form-group form-show-validation row">
		                                    <label class="col-md-2 mt-sm-2 text-right">
		                                        Nama User <span class="required-label">*</span>
		                                    </label>
		                                    <div class="col-md-4">
		                                        <input type="text" class="form-control" name="name" placeholder="Nama User" required>
		                                    </div>
		                                </div>
		                                <div class="form-group form-show-validation row">
		                                    <label class="col-md-2 mt-sm-2 text-right">
		                                        Password <span class="required-label">*</span>
		                                    </label>
		                                    <div class="col-md-4">
		                                    	<div class="form-icon">
			                                        <input type="password" class="form-control" id="password" name="password" placeholder="Password" onchange="" required>
			                                        <span toggle="#password-field" class="fas fa-eye field-icon toggle-password"></span>
		                                    	</div>
		                                    </div>
		                                </div>
		                                <div class="form-group form-show-validation row">
		                                    <label class="col-md-2 mt-sm-2 text-right">
		                                        Ulangi Password <span class="required-label">*</span>
		                                    </label>
		                                    <div class="col-md-4">
		                                    	<div class="form-icon">
			                                        <input type="password" class="form-control" id="cpassword" name="cpassword" placeholder="Ulangi Password" required>
			                                        <span toggle="#password-field" class="fas fa-eye field-icon toggle-cpassword"></span>
		                                    	</div>
		                                        <div class="text-danger mt-2" id="msg-cpassword"></div>
		                                    </div>
		                                </div>
		                                <div class="form-group form-show-validation row">
		                                    <label class="col-md-2 mt-sm-2 text-right">
		                                        Hak Akses <span class="required-label">*</span>
		                                    </label>
		                                    <div class="col-md-4">
		                                        <select class="form-control select2" name="hak_akses" required>
		                                        	<option value="" selected disabled>Pilih Data</option>
		                                        	<option value="1">Admin</option>
		                                        	<option value="2">Staff</option>
		                                        </select>
		                                    </div>
		                                </div>
		                            </div>
		                        </div>
		                    </div>
		                    <div class="card-action">
		                        <div class="row">
		                            <div class="col-md-12">
		                                <button class="btn btn-primary btn-round float-right" type="submit">
		                                	Simpan Data
		                                </button>
		                            </div>
		                        </div>
		                    </div>
		                </form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('javascript')
	<script>
		$('#cpassword').keyup(function (){
	  		// Validate
			var msg = document.getElementById('msg-cpassword');
			var val_password = $('#password').val();
			var val_cpassword = $('#cpassword').val();
			
			if(val_password != val_cpassword)
		    {
		        msg.style.display = "block";
		        msg.innerHTML = "Password tidak sama";
		    }else{
		    	msg.style.display = "none";
		    }
	  	});

	  	$('#password').keyup(function (){
	  		// Validate
			var msg = document.getElementById('msg-cpassword');
			var val_password = $('#password').val();
			var val_cpassword = $('#cpassword').val();
			
			if (val_cpassword != ""){
				if(val_password != val_cpassword)
			    {
			        msg.style.display = "block";
			        msg.innerHTML = "Password tidak sama";
			    }else{
			    	msg.style.display = "none";
			    }
			}
	  	});

	  	$(".toggle-password").click(function() {
			$(this).toggleClass("fa-eye fa-eye-slash");

			var password = document.getElementById('password');
			if (password.type === "password") {
				password.type = "text";
				$('#password_show').text("Hide");
			} else {
				password.type = "password";
				$('#password_show').text("Show");
			}
		});

		$(".toggle-cpassword").click(function() {
			$(this).toggleClass("fa-eye fa-eye-slash");

			var password = document.getElementById('cpassword');
			if (password.type === "password") {
				password.type = "text";
				$('#password_show').text("Hide");
			} else {
				password.type = "password";
				$('#password_show').text("Show");
			}
		});
	</script>
@endsection