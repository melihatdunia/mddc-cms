@extends('layouts.adm_theme', ['activeMenu' => 'categories'])

@section('content')
<div class="content">
	<div class="panel-header bg-primary-gradient">
		<div class="page-inner py-5">
			<div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
				<div>
					<h2 class="text-white pb-2 fw-bold">Tambah Data</h2>
				</div>
				<div class="ml-md-auto py-2 py-md-0">
					<a href="{{ url('categories') }}" class="btn btn-white btn-border btn-round mr-2">
						<i class="fas fa-arrow-left mr-2"></i> Kembali ke List
					</a>
				</div>
			</div>
		</div>
	</div>
	<div class="page-inner mt--5">
		<div class="row mt--2">
			<div class="col-md-12">
				<div class="card full-height">
					<div class="card-body">
						<form action="{{ route('categories.store') }}" method="POST">
		                    {{ csrf_field() }}
		                    <div class="card-body">
		                        <div class="row">
		                            <div class="col-md-12">
		                                <div class="form-group form-show-validation row">
		                                    <label class="col-md-2 mt-sm-2 text-right">
		                                        Category <span class="required-label">*</span>
		                                    </label>
		                                    <div class="col-md-4">
		                                        <input type="text" class="form-control" name="category" placeholder="Category" required>
		                                    </div>
		                                </div>
		                            </div>
		                        </div>
		                    </div>
		                    <div class="card-action">
		                        <div class="row">
		                            <div class="col-md-12">
		                                <button class="btn btn-primary btn-round float-right" type="submit">
		                                	Simpan Data
		                                </button>
		                            </div>
		                        </div>
		                    </div>
		                </form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('javascript')
@endsection