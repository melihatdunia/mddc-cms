@extends('layouts.user_theme', ['active' => strtolower($kategori) ])

@php($DateFormat = 'App\Libraries\Utils\DateFormat')

@section('content')
<div class="warp-title-kategori">
	<div class="title-kategori">
		<span>{{ $kategori }}</span>
	</div>
</div>

@if($total == 0)
	<div class="img-not-result">
		<img src="{{ asset('frontend/images/search-not-found.png') }}">
	</div>
	<div class="title-not-result">
		Maaf, Tidak Ada Artikel.
	</div> 
@else
	<article class="headline">
		<a href="{{ 'read/'.$list[0]->id.'/'.$list[0]->slug }}">
			<div class="img-headline">
				<img src="{{ asset('uploads/articles/'.$list[0]->cover) }}" alt="img-artikel">
			</div>
			<div class="detail-headline">
				<div class="kategori-headline">
					{{ $list[0]->categories->category }}
				</div>
				<div class="title-headline">
					{{ $list[0]->title }}
				</div>
				<div class="box-auth">
					<span class="author">{{ ucfirst($list[0]->author) }}</span>
					<span class="date">{{ $DateFormat::convertToDateId($list[0]->date_publish) }}</span>
				</div>
			</div>
		</a>
	</article>
	<div class="box-artikel">
		@php($start = $number)
		@foreach($list as $key => $rowArtikel)
			@if($key != 0)
				<article class="list-artikel">
					<div class="artikel">
						<a href="{{ 'read/'.$list[$key]->id.'/'.$list[$key]->slug }}">
							<div class="img-artikel">
								<img src="{{ asset('uploads/articles/'.$list[$key]->cover) }}" alt="img-artikel">
							</div>
							<div class="detail-artikel">
								<div class="kategori-artikel">
									{{ $list[$key]->categories->category }}
								</div>
								<div class="title-artikel">
									{{ $list[$key]->title }}
								</div>
								<div class="box-auth">
									<span class="author">{{ ucfirst($list[$key]->author) }}</span>
									<span class="date">{{ $DateFormat::convertToDateId($list[$key]->date_publish) }}</span>
								</div>
							</div>
						</a>
					</div>
				</article>
			@endif

			@php($number++)

		@endforeach
	</div>

	{{ $list->appends(request()->all())->links('vendor.pagination.mddc-paginate') }}
@endif

@endsection