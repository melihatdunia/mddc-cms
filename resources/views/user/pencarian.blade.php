<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="x-dns-prefetch-control" content="on">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="mobile-web-app-capable" content="yes">
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=0">
		<meta name="theme-color" content="#367cbf">
		
		<title>Melihatdunia - Life Is Jurney</title>
		<meta name="title" content=">Melihatdunia - Life Is Jurney">
		<meta name="description" content=">Melihatdunia - Life Is Jurney">
		<meta name="author" content="melihatdunia.com">
		<meta name="image_src" content="">
		<meta name="google-site-verification" content="">
		<meta name="msvalidate.01" content="">
		
		<meta name="googlebot-news" content="index,follow">
		
		<link rel="shortcut icon" href="assets/images/favicon.ico">
		<link rel="icon" type="image/png" href="{{ asset('frontend/images/favicon-192x192.png') }}" sizes="192x192">
        <link rel="icon" type="image/png" href="{{ asset('frontend/images/favicon-152x152.png') }}" sizes="160x160">
        <link rel="icon" type="image/png" href="{{ asset('frontend/images/favicon-96x96.png') }}" sizes="96x96">
        <link rel="icon" type="image/png" href="{{ asset('frontend/images/favicon-32x32.png') }}" sizes="32x32">
        <link rel="icon" type="image/png" href="{{ asset('frontend/images/favicon-16x16.png') }}" sizes="16x16">

		<link rel="stylesheet" href="{{ asset('frontend/css/styles.css') }}">
		<link rel="stylesheet" href="{{ asset('frontend/css/bootstrap.min.css') }}">
		<link rel="stylesheet" href="{{ asset('frontend/font/fontawesome5/css/all.min.css') }}">
	</head>

	<body>
		<div class="pencarian clearfix">
			<a class="close-pencarian" href="{{ url('/') }}"><i class="fas fa-times"></i></a>
			<img src="{{ asset('frontend/images/logo.png') }}">
			<div class="form-cari">
				<form action="{{ url('cari-artikel') }}" method="POST">
					{{ csrf_field() }}
					<input type="text" class="txt-cari" name="cari" placeholder="Cari Artikel..">
					<button class="btn-cari" type="submit"><i class="fas fa-search icon-search"></i></button>
				</form>
			</div>
			<!-- <div class="pencarian-populer">
				<ul>
					<li class="title-pencarian-populer">Pencarian Populer : </li>
					<li><a href="#">Laravel</a></li>
					<li><a href="#">CodeIgniter</a></li>
					<li><a href="#">Bandung</a></li>
				</ul>
			</div> -->
		</div>
	</body>

	<script src="{{ asset('frontend/js/jquery.min.js') }}"></script>
  	<script src="{{ asset('frontend/js/bootstrap.min.js') }}"></script>
  	<script src="{{ asset('frontend/font/fontawesome5/js/all.min.js') }}"></script>
</html>

