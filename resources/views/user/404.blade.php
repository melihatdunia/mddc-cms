<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="x-dns-prefetch-control" content="on">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="mobile-web-app-capable" content="yes">
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=0">
		<meta name="theme-color" content="#367cbf">
		
		<title>Melihatdunia - Life Is Jurney</title>
		<meta name="title" content=">Melihatdunia - Life Is Jurney">
		<meta name="description" content=">Melihatdunia - Life Is Jurney">
		<meta name="author" content="melihatdunia.com">
		<meta name="image_src" content="">
		<meta name="google-site-verification" content="">
		<meta name="msvalidate.01" content="">
		
		<meta name="googlebot-news" content="index,follow">
		
		<link rel="shortcut icon" href="assets/images/favicon.ico">
		<link rel="icon" type="image/png" href="{{ asset('frontend/images/favicon-192x192.png') }}" sizes="192x192">
        <link rel="icon" type="image/png" href="{{ asset('frontend/images/favicon-152x152.png') }}" sizes="160x160">
        <link rel="icon" type="image/png" href="{{ asset('frontend/images/favicon-96x96.png') }}" sizes="96x96">
        <link rel="icon" type="image/png" href="{{ asset('frontend/images/favicon-32x32.png') }}" sizes="32x32">
        <link rel="icon" type="image/png" href="{{ asset('frontend/images/favicon-16x16.png') }}" sizes="16x16">

		<link rel="stylesheet" href="{{ asset('frontend/css/styles.css') }}">
		<link rel="stylesheet" href="{{ asset('frontend/css/bootstrap.min.css') }}">
		<link rel="stylesheet" href="{{ asset('frontend/font/fontawesome5/css/all.min.css') }}">
	</head>

	<body>
		<main>
			<div class="container">
				<div class="not-found-page">
					<div class="img-not-result">
						<img src="{{ asset('frontend/images/search-not-found.png') }}">
					</div>
					<div class="title-not-result">
						Maaf, Halaman Tidak Ditemukan.
					</div> 
					<div class="btn-back-home">
						<a href="{{ URL('/') }}">Kembali Ke Home</a>
					</div>
				</div>
			</div>
		</main>
	</body>
</html>