@extends('layouts.user_theme', ['active' => 'home' ])

@php($DateFormat = 'App\Libraries\Utils\DateFormat')

@section('content')
	<article class="headline">
		<a href="{{ url('read/'.$listArtikel[0]->id.'/'.$listArtikel[0]->slug) }}">
			<div class="img-headline">
				<img src="{{ asset('uploads/articles/'.$listArtikel[0]->cover) }}" alt="img-artikel">
			</div>
			<div class="detail-headline">
				<div class="kategori-headline">
					{{ $listArtikel[0]->categories->category }}
				</div>
				<div class="title-headline">
					{{ $listArtikel[0]->title }}
				</div>
				<div class="box-auth">
					<span class="author">{{ ucfirst($listArtikel[0]->author) }}</span>
					<span class="date">{{ $DateFormat::convertToDateId($listArtikel[0]->date_publish) }}</span>
				</div>
			</div>
		</a>
	</article>
	<div class="box-artikel">
		@php($start = $number)
		@foreach($listArtikel as $key => $rowArtikel)

			@if($key != 0)
				<article class="list-artikel">
					<div class="artikel">
						<a href="{{ url('read/'.$listArtikel[$key]->id.'/'.$listArtikel[$key]->slug) }}">
							<div class="img-artikel">
								<img src="{{ asset('uploads/articles/'.$listArtikel[$key]->cover) }}" alt="img-artikel">
							</div>
							<div class="detail-artikel">
								<div class="kategori-artikel">
									{{ $listArtikel[$key]->categories->category }}
								</div>
								<div class="title-artikel">
									{{ $listArtikel[$key]->title }}
								</div>
								<div class="box-auth">
									<span class="author">{{ ucfirst($listArtikel[$key]->author) }}</span>
									<span class="date">{{ $DateFormat::convertToDateId($listArtikel[$key]->date_publish) }}</span>
								</div>
							</div>
						</a>
					</div>
				</article>
			@endif
			
			@php($number++)

		@endforeach
	</div>

	{{ $listArtikel->appends(request()->all())->links('vendor.pagination.mddc-paginate') }}

	<!-- E Book -->
	@if($ebookUpdate != "" )
	<div class="ebook">
		<div class="clearfix">
			<div class="title-ebook-section">
				<i class="fas fa-book"></i> Ebook Gratis
			</div>
			<div class="more-artikel">
				<a href="{{ url('ebook') }}"> Lainnya <i class="fas fa-arrow-right"></i> </a>
			</div>
		</div>

		
		<div class="list-ebook clearfix">
			<div class="left-ebook-content">
				<div class="title-ebook">
					{{ $ebookUpdate->title }}
				</div>
				<div class="desc-ebook">
					{!! $ebookUpdate->descriptions !!}
				</div>
				<div class="btn-unduh">
					<a href="{{ $ebookUpdate->ebook_link }}" target="_blank"><i class="fas fa-download"></i> Unduh </a>
				</div>
			</div>
			<div class="right-ebook-content">
				<img src="{{ asset('uploads/ebook/'.$ebookUpdate->cover) }}" alt="ebook">
			</div>
		</div>
	</div>
	@endif
@endsection