@extends('layouts.user_theme')

@php($DateFormat = 'App\Libraries\Utils\DateFormat')

@section('content')
	<article class="read-artikel">
		<div class="img-read-artikel">
			<img src="{{ asset('uploads/articles/'.$rowArtikel->cover) }}" alt="img-news">
		</div>
		<div class="detail-read-artikel">
			<div class="kategori-read-artikel">
				{{ $rowArtikel->categories->category }}
			</div>
			<div class="title-read-artikel">
				{{ $rowArtikel->title }}
			</div>
			<div class="box-auth-read-artikel">
				<span style="color: #A9A9A9; font-size: 14px;">Oleh </span>
				<span class="author-read-artikel"><a href="#">{{ ucfirst($rowArtikel->author) }}</a></span>
				<span class="date-read-artikel">{{ $DateFormat::convertToDateId($rowArtikel->date_publish) }}</span>
			</div>
			<div class="desc-read-artikel">
				{!! $rowArtikel->descriptions !!}

				<code class="language-css">p { color: red }</code>
			</div>
			<div class="tag-read-artikel">
				<ul>
					@foreach(explode(',', $rowArtikel->tags) as $tag) 
					<li>
						<a href="{{ 'tags/'.str_replace(' ','-', $tag) }}">
							<i class="fas fa-tags"></i> {{ $tag }}
						</a>
					</li>
					@endforeach
				</ul>
			</div>
			<div class="sharethis-inline-share-buttons"></div>
		</div>
	</article>

	<div class="prev-next">
		<div class="warp-prev-next">
			<div class="row-prev-next">
				@if(@$prevArtikel->id != "")
				<article class="list-prev-next-artikel">
					<div class="prev-next-artikel">
						<a href="{{ url('read/'.@$prevArtikel->id.'/'.@$prevArtikel->slug) }}">
							<div class="img-prev-next-artikel img-prev">
								<img src="{{ asset('uploads/articles/'.@$prevArtikel->cover) }}" alt="img-artikel">
							</div>
							<div class="desc-prev">
								<div class="nav-prev">
									<i class="fas fa-long-arrow-alt-left"></i> Sebelumnya
								</div>
								<div class="title-prev-next-artikel">
									{{ @$prevArtikel->title }}
								</div>
							</div>
						</a>
					</div>
				</article>
				@else
				<div style="width: 50%;"></div>
				@endif

				@if(@$nextArtikel->id != "")
				<article class="list-prev-next-artikel">
					<div class="prev-next-artikel">
						<a href="{{ url('read/'.@$nextArtikel->id.'/'.@$nextArtikel->slug) }}">
							<div class="img-prev-next-artikel img-next">
								<img src="{{ asset('uploads/articles/'.@$nextArtikel->cover) }}" alt="img-artikel">
							</div>
							<div class="desc-next">
								<div class="nav-next">
									Selanjutnya <i class="fas fa-long-arrow-alt-right"></i>
								</div>
								<div class="title-prev-next-artikel">
									{{ @$nextArtikel->title }}
								</div>
							</div>
						</a>
					</div>
				</article>
				@else
				<div style="width: 50%;"></div>
				@endif
			</div>
		</div>
	</div>

	<!-- <div class="komentar">
		<div class="fb-comments" data-href="https://www.facebook.com/melihatduniacom/" data-width="100%" data-numposts="5"></div>
	</div> -->
@endsection

@section('javascript')
	
@endsection