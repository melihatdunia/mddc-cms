@extends('layouts.user_theme', ['active' => '' ])

@php($DateFormat = 'App\Libraries\Utils\DateFormat')

@section('content')
<div class="warp-title-keyword">
	<span class="title-keyword">Hasil Pencarian :</span>
	<span class="keyword">{{ $keyword }}</span>
</div>

@if($total == 0)
	<div class="img-not-result">
		<img src="{{ asset('frontend/images/search-not-found.png') }}">
	</div>
	<div class="title-not-result">
		Maaf, Tidak Ada Artikel.
	</div> 
@else
	<div class="box-pencarian">
		@php($start = $number)
		@foreach($list as $key => $rowArtikel)
			<article class="list-artikel-search">
				<div class="artikel-search">
					<a href="{{ 'read/'.$rowArtikel->id.'/'.$rowArtikel->slug }}">
						<div class="img-artikel-search">
							<img src="{{ asset('uploads/articles/'.$rowArtikel->cover) }}" alt="img-artikel">
						</div>
						<div class="detail-artikel-search">
							<div class="kategori-artikel">
								{{ $rowArtikel->categories->category }}
							</div>
							<div class="title-artikel">
								{{ $rowArtikel->title }}
							</div>
							<div class="box-auth">
								<span class="author">{{ ucfirst($rowArtikel->author) }}</span>
								<span class="date">{{ $DateFormat::convertToDateId($rowArtikel->date_publish) }}</span>
							</div>
						</div>
					</a>
				</div>
			</article>
		@php($number++)
		@endforeach
	</div>

	{{ $list->appends(request()->all())->links('vendor.pagination.mddc-paginate') }}
@endif

@endsection