﻿# Host: localhost  (Version 5.5.5-10.4.19-MariaDB)
# Date: 2021-08-02 06:38:20
# Generator: MySQL-Front 6.0  (Build 2.20)


#
# Structure for table "articles"
#

DROP TABLE IF EXISTS `articles`;
CREATE TABLE `articles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `descriptions` text DEFAULT NULL,
  `date_publish` date DEFAULT NULL,
  `id_categories` int(11) DEFAULT NULL,
  `tags` text DEFAULT NULL,
  `author` varchar(255) DEFAULT NULL,
  `cover` varchar(100) DEFAULT NULL,
  `view` int(11) DEFAULT 0,
  `slug` varchar(255) DEFAULT NULL,
  `status_publish` int(1) DEFAULT NULL COMMENT '1 = Publish, 0 = Unpublish',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4;

#
# Data for table "articles"
#

INSERT INTO `articles` VALUES (1,'Touring Ke Pantai Klayar dan Watu Karung','<p><span style=\"color:#2ecc71\"><strong>Pacitan</strong></span>, Kota yang memiliki berbagai pantai yang cantik dan tidak boleh dilewatkan ketika mengunjungi pacitan. <img alt=\"smiley\" src=\"https://cdn.ckeditor.com/4.16.1/standard-all/plugins/smiley/images/regular_smile.png\" style=\"height:23px; width:23px\" title=\"smiley\" /></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Perjalanan touring kita mulai dari kota Ngawi sebelum shubuh, sekitar jam 4an pagi. Perjalanan menuju Pacitan waktu itu sangat lancar sekali, mungkin karena masih pagi dini hari kali ya.. udara pun masih sangat sejuk sekali. Beberapa kali istrihat untuk sholat shubuh dan makan pagi akhirnya kamipun sampai di Pantai Klayar tujuan pertama kami di kota Pacitan, kota kelahiran bapak presiden kita SBY.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Kurang lebih kami sampai di pantai Klayar pukul 9 Pagi, udara waktu itu lumayan panas dan masih sepi, mungkin karena waktu itu masih pandemi Covid-19 dan baru diizinkan kembali tempat wisata buka. Tanpa membuang waktu kita langsung mencari tempat parkir yang terbilang mudah untuk di dapat disini karena lahan parkir sangat luas dan banyak.</p>\r\n\r\n<p><img src=\"https://live.staticflickr.com/65535/51308127473_8ed2bb9bac_b.jpg\" style=\"width:698.533px\" /></p>\r\n\r\n<h6>(Motor Touring)</h6>\r\n\r\n<h6>&nbsp;</h6>\r\n\r\n<p>Setelah parkir kamipun langsung naik keatas bukit yang ada di pantai Klayar untuk menikmati pemandangan pantai Klayar dari atas.</p>\r\n\r\n<p>Dari atas sini kami bisa menikmati deburan ombak yang menyapu bebatuan di pantai dan juga bisa melihat semburan air laut dari celah-celah batu yang ada di pantai Klayar. Udara panas seakan tertutupi angin yang semilir menerpa kami.</p>\r\n\r\n<p><img src=\"https://live.staticflickr.com/65535/51308939005_1aeb9d3fc9_b.jpg\" style=\"width:698.533px\" /></p>\r\n\r\n<h6>(Foto Dari Atas Bukit Pantai Klayar)</h6>\r\n\r\n<h6>&nbsp;</h6>\r\n\r\n<p><img src=\"https://live.staticflickr.com/65535/51307964536_cdc096c57d_b.jpg\" style=\"width:698.533px\" /></p>\r\n\r\n<h6>(Pantai Klayar dari Atas Bukit)</h6>\r\n\r\n<h6>&nbsp;</h6>\r\n\r\n<p>Setelah puas menikmati pemandangan dari atas, kamipun turun dan menikmati es kelapa untuk menyegarkan tenggorokan karena cuaca yang panas waktu itu.</p>\r\n\r\n<p><img src=\"https://live.staticflickr.com/65535/51308972860_cb0dd32ae8_b.jpg\" style=\"width:698.533px\" /></p>\r\n\r\n<h6>(Es Degan)</h6>\r\n\r\n<h6>&nbsp;</h6>\r\n\r\n<p>Setelah puas di pantai Klayar, kami pun lanjut ke destinasi berikutnya yaitu pantai Watu Karung, pantai Watu Karung tidak kalah indah dari pantai Klayar. Pantai Watu Karung memiliki pasir yang luas dan masih banyak kelomangan kecil-kecil yang bisa kita lihat secara langsung.</p>\r\n\r\n<p>Kalau di pantai Klayar kita tidak bisa bermain air karena ombak waktu itu cukup tinggi sehingga dilarang untuk main air di laut, di pantai Watu Karung kita bisa bermain air sepuasnya karena disini ombak tidak terlalu besar dan ada spot yang bagus untuk menikmati air laut yang asin :D</p>\r\n\r\n<p><img src=\"https://live.staticflickr.com/65535/51307940566_953e575bbb_b.jpg\" style=\"width:698.533px\" /></p>\r\n\r\n<h6>(Main Air Laut)</h6>\r\n\r\n<h6>&nbsp;</h6>\r\n\r\n<p>Tidak hanya bermain air, kita pun bisa menikmati pemandangan pantai Watu Karung dari atas bukit lho. Pemandangan disini juga tidak kalah bagus, kita bisa melihat lautan luas secara langsung dan perahu-perahu nelayan yang sedang mencari ikan</p>\r\n\r\n<p><img src=\"https://live.staticflickr.com/65535/51307196682_6f112144dd_b.jpg\" style=\"width:698.533px\" /></p>\r\n\r\n<h6>(Pemandangan Pantai Watu Karung)</h6>\r\n\r\n<h6>&nbsp;</h6>\r\n\r\n<p>Itulah sekilas touring kami di kota Pacitan, yang lumayan melelahkan tapi tidak terlupakan.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Biaya Masuk Pantai Klayar :</strong></p>\r\n\r\n<p>Tiket Masuk :</p>\r\n\r\n<ul>\r\n\t<li>Dewasa : Rp. 15.000</li>\r\n\t<li>Anak-anak : Rp. 10.000</li>\r\n</ul>\r\n\r\n<p>Parkir :</p>\r\n\r\n<ul>\r\n\t<li>Motor : Rp. 2.000</li>\r\n\t<li>Mobil : Rp. 5.000</li>\r\n</ul>\r\n\r\n<p><strong>Biaya Masuk Pantai Watu Karung :</strong></p>\r\n\r\n<p>Tiket Masuk :</p>\r\n\r\n<ul>\r\n\t<li>Dewasa : Rp. 10.000</li>\r\n\t<li>Anak-anak : Rp. 5.000</li>\r\n</ul>\r\n\r\n<p>Parkir :</p>\r\n\r\n<ul>\r\n\t<li>Motor : Rp. 2.000</li>\r\n\t<li>Mobil : Rp. 5.000</li>\r\n</ul>','2021-03-21',3,'klayar,watu karung,traveling','administrator','cover_touring_ke_pantai_klayar.jpg',80,'touring-ke-pantai-klayar-dan-watu-karung',1,'2021-07-13 03:44:22','2021-07-31 23:04:41'),(2,'Menjelajah Eropa Di Devoyage','<p><b>Devoyage Bogor,</b> tempat wisata yang bisa membuat kita merasakan bagaimana mengunjungi tempat-tempat terkenal di Eropa. Disini ada berbagai replika tempat iconic yang ada di berbagai negara Eropa, seperti Menara Eifel, <span>Cinque Terre</span>, Kincir Angin Belanda, Jam Big Ben, dll.</p><p>Tempat ini sangat cocok bagi kalian yang ingin liburan bersama keluarga, selain instagramable juga disini ada wahana permainan yang bisa kita nikmati, seperti perahu kano, house of game yang merupakan wahana labirin, dan ada juga komidi putar untuk anak-anak.</p><p><b>Wahana Kano</b></p><p>Bagi kalian yang ingin mencoba mengarungi sungai dengan kano, kalian bisa mencoba wahana ini. Wahana kano ini bisa dinaiki untuk 2 orang dewasa, atau maksimal 3 orang dengan 1 anak kecil. Wahana ini sangat seru loh karena kita di tuntut untuk saling kompak agar perahu tetap bisa berjalan dengan baik dan melewai berbagai belokan alirang sungai. </p><p><img style=\"width: 698.533px;\" src=\"https://live.staticflickr.com/65535/51309268885_1e866e3b8d_b.jpg\"><br></p><div align=\"center\"><h6><span style=\"font-weight: normal;\">(Perahu Kano)</span></h6></div><div align=\"center\"><br></div><div align=\"left\"><b>House Of Game</b></div><div align=\"left\"><b><br></b></div><div align=\"left\">Ini juga salah satu wahana yang sangat menarik menurut saya, wahana ini memberikan pengalaman kita untuk melewati berbagai rintangan di dalam labirin. Banyak pintu-pintu yang tersembunyi di dalamnya sehingga di perlukan ketelitian agar kita tidak tersesat di dalamnya.</div><div align=\"left\"><br></div><div align=\"left\"><img style=\"width: 698.533px;\" src=\"https://live.staticflickr.com/65535/51308481543_055c9d4115_b.jpg\"><br><br></div><div align=\"left\"><div align=\"center\"><h6><span style=\"font-weight: normal;\">(House of Game)</span></h6></div><div align=\"center\"><br></div><div align=\"left\">Selain wahana-wahana diatas juga kita bisa berfoto di berbagai spot bernuansa negara-negara Eropa, kapan lagi coba bisa keliling Eropa di satu tempat aja. :D</div><div align=\"left\"><br></div><div align=\"left\"><img style=\"width: 698.533px;\" src=\"https://live.staticflickr.com/65535/51307521837_6151ceaa1c_b.jpg\"><br></div><div align=\"left\"><br></div><div align=\"center\"><h6><span style=\"font-weight: normal;\">(Jam Big Ben)</span></h6><h6><b><br></b></h6></div><div align=\"center\"><img style=\"width: 683px;\" src=\"https://live.staticflickr.com/65535/51308258921_b87dba9e14_b.jpg\"><br></div><br></div><div align=\"center\"><h6><span style=\"font-weight: normal;\">(Menara Eifel)</span></h6></div><div align=\"center\"><br></div><div align=\"left\">Pokoknya banyak deh spot foto yang seru buat mengisi Instagram kamu.</div><div align=\"left\"><br></div><div align=\"left\"><b>Tiket Masuk :</b></div><div align=\"left\"><b><br></b></div><ul><li>Weekday : Rp. 30.000</li><li>Weekend : Rp. 40.000</li><li>Terusan ( + 2 Wahana Permainan ) : Rp. 75.000<br></li></ul><p><br><b><br></b><br></p>','2020-11-15',3,'devoyage,bogor','administrator','cover_menjelajah_eropa_di_devoyage.jpg',13,'menjelajah-eropa-di-devoyage',1,'2021-07-13 07:52:34','2021-07-29 05:14:20'),(3,'Menyeberangi Jembatan Suspension Situ Gunung','<p><b>Situ Gunung, </b>tempat wisata alam yang sangat menarik untuk di kunjungi jika kita sedang di Sukabumi, Jawa barat. Bagi kalian pecinta alam yang suka menelusuri rimbunnya hutan, tempat ini cocok juga untuk kalian jelajahi. Perjalanan di Situ Gunung di mulai dengan menelusuri jalan berbatu yang sudah tertata rapih dan di kelilingi pepohonan yang rimbun.</p><p>Setelah berjalan beberapa menit kita sampai di pos tiket masuk. Setelah membayar tiket masuk dan berjalan lagi beberapa menit, kita akan sampai di pemberhentian pertama kita yaitu <b>Teater Budaya</b>. Disini kita akan di beri welcome drink dan makan ringan seperti singkong rebus, dll. Di Teater ini pula kita bisa melihat pertunjukan seperti tarian-tarian dan lagu-lagu khas daerah jawa barat.</p><p><img style=\"width: 698.533px;\" src=\"https://live.staticflickr.com/65535/51309159322_8d285224ba_b.jpg\"></p><h6 align=\"center\"><span style=\"font-weight: normal;\">(Teater Pertunjukan)</span></h6><h6 align=\"center\"><b><br></b></h6><p align=\"center\"><img style=\"width: 698.533px;\" src=\"https://live.staticflickr.com/65535/51309160087_4c90ef3f7e_b.jpg\"></p><h6 align=\"center\"><span style=\"font-weight: normal;\">(Menikmati Welcome Drink)</span></h6><h6 align=\"center\"><b><br></b></h6><div align=\"left\">Setelah puas menikmati pertunjukan di Teater Budaya dan menyeruput hangatnya welcome drink, kita melanjutkan ke destinasi kita selanjutnya yaitu <b>Suspension Bridge (Jembatan Gantung)</b>. Jembatan gantung ini terbentang sepanjang 243 meter dan menjadi jembatan gantung&nbsp; terpanjang di Asia Tenggara. Kalian tidak perlu takut untuk melewati jembatan ini karena sudah ada tali pengaman yang bisa digunakan agar kita bisa lebih seimbang, karena memang kalau sudah sampai di tengah jembatan, jembatan tersebut agak bergoyang ketika banyak yang melewati. Pemandangan disekitar Jembatan Gantung sangat bagus sekali, kita bisa melihat hutan rimbun dibawah jembatan gantung ini.</div><div align=\"left\"><br></div><div align=\"left\"><img style=\"width: 698.533px;\" src=\"https://live.staticflickr.com/65535/51309909251_93aee29de4_b.jpg\"></div><div align=\"left\"><br></div><div align=\"center\"><h6><span style=\"font-weight: normal;\">(Antrian Melewati Suspension Bride)</span></h6></div><div align=\"center\"><br><b><img src=\"https://live.staticflickr.com/65535/51309911986_25a1c7e4e0_b.jpg\"></b></div><div align=\"center\"><h6><b><br></b></h6><h6><span style=\"font-weight: normal;\">(Suspension Bridge)</span></h6></div><div align=\"center\"><b><br></b></div><div align=\"left\">Setelah kita melewati jembatan gantung ini, kita akan sampai di destinasi terakhir kita yaitu <b>Curug Sawer</b>. Curug yang terletak di taman wisata Situ Gunung ini menjadi tempat terakhir yang bisa kita nikmati. Disini kita bisa melihat keindahan curug dan merasakan main di sungai di sekitar Curug Sawer. Curug Sawer memiliki ketinggian sekitar 35 Meter dan mengalirkan air yang masih jernih, sayangnya kita hanya bisa melihatnya saja, karena tidak boleh berenang atau mandi di sini karena dilarang.</div><div align=\"left\"><br></div><div align=\"left\"><img style=\"width: 698.533px;\" src=\"https://live.staticflickr.com/65535/51310915155_6beeeb12bc_b.jpg\"></div><div align=\"left\"><br></div><div align=\"center\"><h6><span style=\"font-weight: normal;\">(Curug Sawer)</span></h6></div><div align=\"center\"><b><br></b></div><div align=\"center\"><div align=\"left\">Nah itulah cerita singkat kami berpetualang di Situ Gunung, untuk informasi tambahan di sini juga ada perkemahan yang bisa digunakan untuk menginap.</div><div align=\"left\"><br></div><div align=\"left\"><div align=\"left\"><b>Tiket Reribusi </b><b>Situ Gunung :</b></div><div align=\"left\"><b><br></b></div><ul><li>Perorang : Rp. 18.500<br><b></b></li></ul></div><div align=\"left\"><br></div><div align=\"left\"><b>Tike Masuk </b><b>(Melewati Jembatan sampai ke Curug) :</b></div><div align=\"left\"><b><br></b></div><div align=\"left\"><ul><li>Dewasa : Rp. 50.000</li><li>Anak-anak : Rp. 25.000</li></ul><p><b>Parkir :</b></p><ul><li>Motor : Rp. 5.000</li><li>Mobil : Rp. 10.000<br></li></ul></div><br></div>','2020-08-15',3,'situ gunung,sukabumi','administrator','cover_menyeberangi_jembatan_suspension_situ_gunung.jpg',7,'menyeberangi-jembatan-suspension-situ-gunung',1,'2021-07-13 11:47:54','2021-07-29 05:14:11'),(4,'Menginap di Ujung Genteng','<p><b>Ujung Genteng, </b>pantai yang terletak di kabupaten Sukabumi ini&nbsp; menawarkan pemandangan yang sangat indah dengan berbagai fasilitas dan tempat makan yang banyak. Perjalanan kami di mulai dari Sukabumi dan menempuh perjalanan kurang lebih 3 jam untuk sampai ke Pantai Ujung Genteng. Tapi jauhnya perjalanan langsung terbayar ketika kita sudah sampai disana, deru ombak dan desiran angin laut menyambut ketika kami sampai disana. </p><p>kami langsung mencari penginapan yang cocok dikantong para nekad traveler ini, maklum lah budget tipis :D. Setelah kita menemukan penginapan yang lumayan murah, kami lalu membereskan tas kami dan segera menuju ke tepi pantai. Saat itu kami sampai sudah lumayan sore, makanya kami tidak mau buang-buang waktu, dan langsung ke tepi pantai untuk menikmati sunset yang terbenam pada sore itu. Sebenarnya pada sore itu ada pelepasan tukik atau anak penyu, tapi ternyata kami terlambat untuk menyaksikannya.. sedih sih, tapi ya memang perjalanan kami lumayan jauh jadi tidak bisa mengejar waktu pelepasan tukik itu.</p><p><img style=\"width: 698.533px;\" src=\"https://live.staticflickr.com/65535/51311260845_9ff12a9127_b.jpg\"></p><h6 align=\"center\"><span style=\"font-weight: normal;\">(Menunggu Sunset)</span></h6><h6 align=\"center\"><b><br></b></h6><div align=\"left\"><img style=\"width: 698.533px;\" src=\"https://live.staticflickr.com/65535/51310980209_14c5875887_b.jpg\"><br></div><p align=\"center\"><br></p><div align=\"left\">Setelah puas menikmati sunset dan suasana sore di pantai, kami langsung menuju penginapan yang tidak jauh dari tepi pantai. Malam itu suara deru ombak menabrak karang pantai terdengar sangat jelas sekali, malam itu kami lewati sambil ngobrol ngalor ngidul dan sambil menikmati buah semangka dari si empunya penginapan, seru deh pokoknya apalagi ini pertama kalinya menginap di pantai.</div><div align=\"left\"><br></div><div align=\"left\">Pagi harinya setelah sarapan kami langsung menuju pantai kembali, untuk membuat video dan berfoto dan ga lupa juga main air laut, buat temen-temen yang mau main di pantai harus berhati - hati ya.. usahakan main air yang masih dekat dengan tepian pantai.</div><div align=\"left\"><br></div><div align=\"left\"><img style=\"width: 698.533px;\" src=\"https://live.staticflickr.com/65535/51310308606_b5636e2b25_b.jpg\"></div><div align=\"left\"><br></div><div align=\"center\"><h6><span style=\"font-weight: normal;\">(Bermain Air Dipantai)</span></h6></div><div align=\"center\"><b><br></b></div><div align=\"center\"><div align=\"left\"><img style=\"width: 698.533px;\" src=\"https://live.staticflickr.com/65535/51310258356_a73d351894_b.jpg\"><br></div><div align=\"left\"><br></div><div align=\"center\"><h6><span style=\"font-weight: normal;\">(Penginapan)</span></h6></div><div align=\"center\"><b><br></b></div><div align=\"center\"><div align=\"left\">Nah itulah sekilas perjalanan kami di Pantai Ujung Genteng, Untuk kalian yang sedang ada di Sukabumi bolehlah kalian mampir kesini. Penginapannya juga lumayan murah kalau sudah kenal sama yang punya :D</div><div align=\"left\"><br></div><div align=\"left\"><b>Tiket Masuk :</b></div><div align=\"left\"><b><br></b></div><div align=\"left\"><ul><li>Perorang : Rp. 13.000</li></ul><p><b>Penginapan :</b></p><ul><li>Permalam : Rp. 200.000<br><br></li></ul></div></div><br></div>','2020-08-16',3,'ujung genteng,sukabumi','administrator','cover_menginap_di_ujung_genteng.jpg',11,'menginap-di-ujung-genteng',1,'2021-07-14 04:39:26','2021-07-29 05:14:16'),(5,'Padang Rumput Bukit Golf Cibodas','<p><b>Cibodas,</b> gabut banget weekend bingung mau kemana, niatnya mau ke Cibeureum tapi ternyata lagi di tutup Gunung Gede Pangrangonya. Untuk mengobati rasa kecewa yaudah kita lanjut aja ke Bukit Golf Cibodas. Bukit Golf Cibodas adalah padang rumput luas yang berada di wilayah Cibodas dan menghadap langsung ke Gunung Gede Pangrango.</p><p>Perjalanan ke Bukit Golf sama seperti kita mau menuju wilayah Cibodas, <br></p>','2020-06-02',3,'cibodas,bukit golf','administrator','cover_menikmati_padang_rumput_bukit_golf_cibodas.jpg',0,'padang-rumput-bukit-golf-cibodas',2,'2021-07-14 05:21:02','2021-07-23 07:26:54'),(9,'Tes','<p>Hallo guys. Kali ini saya mau sharing tentang bagaimana kita menghilangkan space kosong jika <b>google adsense</b> yang kita letakkan di web kita unfilled (tidak ada iklan yang mengisi). Okey tidak perlu panjang lebar, ini adalah cara simple dan sangat mudah untuk&nbsp; diterapkan :D.</p><p>Kita hanya perlu menambahkan CSS sebagai berikut di halaman HTML atau di file CSS :</p><pre>&lt;style&gt;<br>&nbsp;&nbsp; &nbsp;ins[data-ad-status=unfilled] {display:none!important}<br>&lt;/style&gt;<br></pre><p>Nah.. mudah bukan.. silahkan dicoba.</p><pre class=\" d-flex flex-wrap flex-items-center break-word f3 text-normal\"><strong itemprop=\"name\" class=\"mr-2 flex-self-stretch\">  </strong></pre>','2021-07-20',3,'coding','administrator','cover_tes.jpg',35,'tes',1,'2021-07-25 07:22:19','2021-07-29 07:29:07'),(10,'html',NULL,'2021-07-29',2,NULL,'administrator','cover_html.jpg',3,'html',1,'2021-07-29 06:38:32','2021-07-29 07:28:53'),(11,'tes','<pre>\r\n<code class=\"language-html\">&lt;!-- CSS Files --&gt;\r\n&lt;link rel=\"stylesheet\" href=\"{{ asset(\'atlantis/css/bootstrap.min.css\') }}\"&gt;\r\n&lt;link rel=\"stylesheet\" href=\"{{ asset(\'atlantis/css/atlantis.min.css\') }}\"&gt;</code></pre>\r\n\r\n<pre>\r\n<code class=\"language-css\">.desktop{\r\n\tdisplay: block;\r\n}\r\n\r\n.mobile{\r\n\tdisplay: none;\r\n}</code></pre>\r\n\r\n<p>&nbsp;</p>','2021-07-28',2,NULL,'administrator','cover_tes.jpg',20,'tes',1,'2021-07-29 06:42:36','2021-07-29 08:35:42');

#
# Structure for table "categories"
#

DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;

#
# Data for table "categories"
#

INSERT INTO `categories` VALUES (2,'Programming','2021-07-09 02:45:52','2021-07-23 07:51:32'),(3,'Traveling','2021-07-13 03:43:26','2021-07-13 03:43:26'),(4,'Tips','2021-07-19 14:52:16','2021-07-19 14:52:16'),(5,'Opini','2021-07-19 14:52:23','2021-07-19 14:52:23'),(6,'Ebook','2021-07-19 14:52:35','2021-07-19 14:52:35');

#
# Structure for table "ebook"
#

DROP TABLE IF EXISTS `ebook`;
CREATE TABLE `ebook` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `date_publish` date DEFAULT NULL,
  `author` varchar(255) DEFAULT NULL,
  `cover` varchar(100) DEFAULT NULL,
  `descriptions` text DEFAULT NULL,
  `ebook_link` text DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `status_publish` int(1) DEFAULT NULL COMMENT '1 = Publish, 0 = Unpublish',
  `total_download` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

#
# Data for table "ebook"
#

INSERT INTO `ebook` VALUES (1,'Vue.js Progresive JS Framework','2021-07-19','administrator','cover_vue.js_progresive_js_framework.png','<p>Mengenal Vue.js dari pengenalan sejarah, dasar-dasar vue dan penerapan vue pada project aplikasi yang kita bangun sampai proses deployment pada hosting<br></p>','https://drive.google.com/file/d/1BfMpFuyq3slLWufjW-S8WgXrxbfc63Bi/view?usp=sharing','vue.js-progresive-js-framework',2,NULL,'2021-07-19 04:02:47','2021-07-23 22:50:28');

#
# Structure for table "search_keyword"
#

DROP TABLE IF EXISTS `search_keyword`;
CREATE TABLE `search_keyword` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `keyword` varchar(255) DEFAULT NULL,
  `total_search` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

#
# Data for table "search_keyword"
#


#
# Structure for table "users"
#

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hak_akses` int(1) DEFAULT NULL COMMENT '1 : Admin, 2 : Contributor',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

#
# Data for table "users"
#

INSERT INTO `users` VALUES (1,'administrator','admin@gmail.com','$2y$10$aoIFMG1uuf.MAp.NOvwnBeh4mBY39bekTTKUfujFI/JVhFj6H/3tq',1,NULL,NULL,'2021-06-24 06:27:43'),(5,'Puji Astuti','puji@gmail.com','$2y$10$ZAQpWiur3iuHqnJh0x1aP.hkKOy21ffvp5ZMyRG9vTVOEbES7qEqC',1,NULL,'2021-07-23 07:52:06','2021-07-23 07:52:06');

#
# Structure for table "youtube"
#

DROP TABLE IF EXISTS `youtube`;
CREATE TABLE `youtube` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `date_publish` date DEFAULT NULL,
  `author` varchar(255) DEFAULT NULL,
  `cover` varchar(100) DEFAULT NULL,
  `youtube_link` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `status_publish` int(1) DEFAULT NULL COMMENT '1 = Publish, 0 = Unpublish',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

#
# Data for table "youtube"
#

INSERT INTO `youtube` VALUES (1,'Wisata Bernuansa Eropa, Devoyage','2021-07-19','administrator','cover_wisata_bernuansa_eropa,_devoyage.jpg','https://youtu.be/6wYxImhwns8','wisata-bernuansa-eropa,-devoyage',1,'2021-07-19 04:34:55','2021-07-23 07:50:03'),(2,'Fajar dan Senja, Ujung Genteng','2021-07-19','administrator','cover_fajar_dan_senja,_ujung_genteng.jpg','https://youtu.be/D9BhrPYNpLI','fajar-dan-senja,-ujung-genteng',1,'2021-07-19 04:48:30','2021-07-19 04:48:30');
