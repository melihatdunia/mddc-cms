<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Auth
Route::get('mddc-login', 'LoginController@index');
Route::get('login', 'LoginController@index')->name('login');
Route::post('authlogin', 'LoginController@authentication');
Route::get('logout', 'LoginController@logout');

// Frontend
Route::get('/', 'User\HomeController@index');
Route::get('read/{id}/{slug}', 'User\ArticlesController@read');
Route::get('programming', 'User\ProgrammingController@index');
Route::get('traveling', 'User\TravelingController@index');
Route::get('tips', 'User\TipsController@index');
Route::get('masakan', 'User\MasakanController@index');
Route::get('opini', 'User\OpiniController@index');
Route::get('ebook', 'User\EbookController@index');

Route::get('cari', 'User\CariController@index');
Route::post('cari-artikel', 'User\CariController@cari');

Route::middleware('auth')->group(function () {

	// Admin
	Route::get('admin', 'Admin\HomeController@index');
	Route::resource('users', 'Admin\UserController');
	Route::resource('categories', 'Admin\CategoriesController');
	Route::resource('articles', 'Admin\ArticlesController');
	Route::resource('ebooks', 'Admin\EbookController');
	Route::resource('youtube', 'Admin\YoutubeController');
});